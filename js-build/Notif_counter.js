var React = window.React ? window.React : require('react');
var axios    = require('axios');
var socket = io.connect('http://matcha.bbichero.com:8080');

class Notif_counter extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            notif_counter: this.props.count,
            counter_div: null,
        };
        if (this.state.notif_counter > 9)
            this.state.notif_counter = "9+";

        this.handleNotifDiv        = this.handleNotifDiv.bind(this);
        this.handleCheckNotifState = this.handleCheckNotifState.bind(this);
        this.handleCheckNotifState = this.handleCheckNotifState.bind(this);
        socket.emit('connection_notif', this.props.src_username);
    }

    componentDidMount()
    {
        socket.on('notif_number', this.handleCheckNotifState);
    }

    handleCheckNotifState(type, message, event)
    {
        if (this.state.notif_counter < 9)
        {
            this.state.notif_counter += 1;
            this.state.notif_counter = this.state.notif_counter;
        }
        else if (this.state.notif_counter === 9)
            this.state.notif_counter = "9+";

        this.setState({ notif_counter: this.state.notif_counter });
    }

    // handleNotifDiv()
    // {
    //     if (this.state.notif_counter > 0 || this.state.notif_counter === "9+")
    //     {
    //         return(
    //             <div>
    //                 <div className="rounded"> {this.state.notif_counter} </div> Settings
    //             </div>
    //         );
    //     }
    //     else
    //         return(<div> Settings </div>);
    // }

    handleNotifDiv()
    {
        if (this.state.notif_counter > 0 || this.state.notif_counter === "9+")
        {
            this.state.counter_div =
            React.createElement("div", null, 
                React.createElement("div", {className: "rounded"}, " ", this.state.notif_counter, " "), " Settings"
            );
        }
        else
            this.state.counter_div = React.createElement("div", null, " Settings ");
    }

// ***************************************************************************** //
// This is the filter part who brings an adaptive result by the user parameters. //
// ***************************************************************************** //

    render()
    {
        return (
            React.createElement("div", null, 
                this.handleNotifDiv(), 
                this.state.counter_div
            )
        );
    }
}

if (typeof module != 'undefined')
{
    module.exports = Notif_counter;
}
else
{
    window.Notif_counter = Notif_counter;
}

// xxxxxxxxxxxx //
// End of page. //
// xxxxxxxxxxxx //
