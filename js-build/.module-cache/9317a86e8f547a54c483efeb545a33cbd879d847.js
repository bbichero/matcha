var React = window.React ? window.React : require('react');
var axios = require('axios');
var socket = io.connect('http://matcha.bbichero.com:8080');

class Conversation extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            messages_list: this.props.messages_list,
            messages_number: Object.keys(this.props.messages_list).length,
            active: false,
            notification: true,
            src_username: this.props.src_username,
            dst_username: this.props.dst_username,
            current_message: "",
            profile_prez: this.props.profile_prez,
            data: {},
            last_message: null,
            message_status: '',
        };

        // Set message state
        if (this.state.messages_number > 1)
        {
            this.state.messages_list.forEach((element, index) =>
            {
                this.state.messages_list[index].mouseOn = false;
                if (this.state.messages_list[index].status === 'N')
                    this.state.message_status = false;
                else if (this.state.messages_list[index].status === 'R')
                    this.state.message_status = 'received';
            });
        }
        this.sendMessage              = this.sendMessage.bind(this);
        this.handleReceiptOf          = this.handleReceiptOf.bind(this);
        this.updateCurrentMessage     = this.updateCurrentMessage.bind(this);
        this.handleReceiveMessage     = this.handleReceiveMessage.bind(this);
        this.handleSeenConversation   = this.handleSeenConversation.bind(this);
        this.displayMessagesSection   = this.displayMessagesSection.bind(this);
        this.handleConversationStatus = this.handleConversationStatus.bind(this);

        socket.emit('connection_user', this.props.src_username);
    }

    componentDidMount()
    {
        socket.on('message', this.handleReceiveMessage);
        socket.on('seen', this.handleSeenConversation);
        socket.on('message_receipt_of', this.handleReceiptOf);
    }
    componentDidUpdate()
    {
        if (this.state.active === true)
        {
            var big = document.getElementById("big");
            big.scrollTop = big.scrollHeight;
        }
    }

    updateCurrentMessage(event)
    {
        this.setState({current_message: event.target.value});
    }

    updateMessageNumber()
    {
        this.setState({messages_lenght: Object.keys(this.state.messages_list).length});
    }

    handleConversationStatus(event)
    {
        if (this.state.active === true)
            this.setState({active: false});
        else {
            if (this.state.src_username !== this.state.last_message.src_username)
                socket.emit('seen', {dst_username: this.state.last_message.dst_username, src_username: this.state.last_message.src_username});
            this.setState({active: true});
        }
    }

    deleteMessage(message, index, event)
    {
        let del_message = 'delete_message=' + message.id +
                        '&src_username=' + this.state.src_username;

        if (this.state.src_username === message.src_username)
        {
            axios(
            {
                method: 'POST',
                url: 'http://dev.matcha.bbichero.com/post.php',
                data: del_message
            }).then((response) =>
            {
                // Check id deleted in parameter with response id from post.php
                // Check index of tab pass in parameter with index deleted from array list
                let check = this.state.messages_list.splice(index, 1)[0];
                if (response.data.id_del_message == message.id && check === message)
                {
                    if (this.state.messages_list.length === 0)
                        this.setState({active: false});
                    else {
                        this.setState(
                        {
                            messages_list: this.state.messages_list,
                            messages_lenght: this.state.messages_lenght - 1,
                        });
                    }
                }
            });
        }
    }

    handleReceiptOf(infos, event)
    {
        if (infos.message.src_username === this.state.src_username &&
            infos.message.dst_username === this.state.dst_username)
        {
            if (infos.active === true)
                this.setState({ message_status: 'seen' });
            else
                this.setState({ message_status: 'received' });
        }
    }

    handleSeenConversation(conversation, event)
    {
        if (conversation.dst_username === this.state.dst_username)
            this.setState({ message_status: 'seen' });
    }

    handleReceiveMessage(message, event)
    {
        var new_list = this.state.messages_list.slice();
        if (message.body.length > 0 && message.src_username === this.state.dst_username)
        {
            new_list.push(message);
            this.setState(
            {
                messages_list: new_list,
                messages_lenght: this.state.messages_lenght + 1,
            });
            socket.emit('message_receipt_of', {message: message, active: this.state.active});
        }
    }

    sendMessage(event)
    {
        let message = 'send_message=' + this.state.current_message + '&src_username=' + this.state.src_username + '&dst_username=' + this.state.dst_username;
        axios(
        {
            method: 'POST',
            url: 'http://dev.matcha.bbichero.com/post.php',
            data: message
        }).then((response) =>
        {
            var new_list = this.state.messages_list.slice();
            if (response.data.body)
            {
                new_list.push(response.data);
                socket.emit('message', response.data, {id_dst_username: this.props.id_dst_username, id_src_username: this.props.id_src_username}); // Transmet le message aux autres
                this.setState(
                {
                    messages_list: new_list,
                    messages_lenght: this.state.messages_lenght + 1,
                    current_message: "",
                    message_status: "false",
                });
            }
        });
    }

    displayDeleteIcon(message, index)
    {
        if (this.state.src_username === message.src_username)
            return (
                React.createElement("div", {className: "outer_big", onClick: this.deleteMessage.bind(this, message, index)}, 
                    React.createElement("div", {className: "cross"})
                )
            );
    }

    displayMessages(messages_list)
    {
        let messages_key = Object.keys(messages_list);
        let last_message = messages_key.slice(-1)[0];
        this.state.last_message = messages_list[last_message];

        const messages = messages_list.map((message, index) =>
        {
            return (
                React.createElement("div", {key: index, className: "chat_container"}, 
                    React.createElement("div", {onClick: this.handleConversationStatus}, 
                        React.createElement("div", {className: "conversation_big"}, 
                            React.createElement("div", {className: (this.state.src_username === message.src_username) ? "user_src" : "user_dst"}, message.src_username), 
                            React.createElement("div", {className: "bubbles"}, 
                                React.createElement("div", {className: (this.state.src_username === message.src_username) ? "me bubble" : "them bubble"}, 
                                    React.createElement("span", {className: "fill_bubble"}, message.body)
                                )
                            )
                        )
                    ), 
                    this.displayDeleteIcon(message, index)
                )
            );
        });

        if (this.state.active === true) {
            return messages;
        }

        else if (messages_list.length > 0)
        {
            return (
                React.createElement("div", {onClick: this.handleConversationStatus}, 
                    React.createElement("div", {className: "conversation"}, 
                        React.createElement("div", {className: "conversation_pp", style: {background: 'url(' + this.state.profile_prez + ')', backgroundSize: 'cover'}}), 
                        React.createElement("div", {className: "conversation_user_dst"}, this.state.dst_username), 
                        React.createElement("div", {className: "last_message"}, messages_list[last_message].body)
                    )
                )
            );
        }
    }

    displayMessageStatus()
    {
        if (this.state.last_message && this.state.src_username === this.state.last_message.src_username)
            return (React.createElement("div", {className: "seen", value: "test"}, " ", (this.state.message_status == "seen") ? "Seen" : ((this.state.message_status == "received") ? "Received" : "Sent")));

        else
            return (React.createElement("div", {className: "seen"}));
    }

    displayMessagesSection()
    {
        if (this.state.active === true)
            return (
                React.createElement("div", null, 
                    this.displayMessages(this.state.messages_list), 
                    React.createElement("div", {className: "sending"}, 
                        this.displayMessageStatus(), 
                        React.createElement("textarea", {className: "imput_text", value: this.state.current_message, onChange: this.updateCurrentMessage}), 
                        React.createElement("div", {className: "send_button fifth", onClick: this.sendMessage}, "Send")
                    )
                )
            );

        else
            return (
                React.createElement("div", null, 
                    React.createElement("div", {id: "trigger_conv"}, 
                        this.displayMessages(this.state.messages_list)
                    )
                )
            );
    }

    render()
    {
        return(
            React.createElement("div", null, 
                this.displayMessagesSection()
            )
        );
    }
}

if (typeof module != 'undefined') {
    module.exports = Conversation;
} else {
    window.Conversation = Conversation;
}
