var React = window.React ? window.React : require('react');
var axios = require('axios');

class Matches extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            profile_prez: this.props.profile_prez,
            id_matched: this.props.id_matched,
            user_matched: this.props.user_matched,
            user_src: this.props.user_src,
            create_conv: false,
            active: false,
        };

        this.displayMatch       = this.displayMatch.bind(this);
        this.createConversation = this.createConversation.bind(this);
    }

    handleCover(state, event)
    {
        var temp       = document.getElementById("match" + this.state.id_matched);
        var display    = document.getElementById("match_button" + this.state.id_matched);
        var match_name = document.getElementById("t_profile" + this.state.id_matched);

        if (state === 'on') {
            temp.classList.add("cover");
            display.style.display  = "";
            match_name.style.color = "white";
        }
        else {
            temp.classList.remove("cover");
            display.style.display  = "none";
            match_name.style.color = "black";
        }
    }

    createConversation(event)
    {
        let message = 'open_message=' + "Start to chat with"  +
                        '&src_username=' + this.state.user_src +
                        '&dst_username=' + this.state.user_matched;
        axios(
        {
            method: 'POST',
            url: 'http://matcha.bbichero.com/post.php',
            data: message
        }).then((response) =>
        {
            if (response.data === "started")
                alert("Conversation already started !");
            else
                location.reload();
        });
    }

    displayMatch()
    {
        let profile_link = "http://dev.matcha.bbichero.com/public_profile.php?profile_username=" + this.props.user_matched;

        return (
            React.createElement("div", {id: "match" + this.state.id_matched, className: "conversation", onMouseOver: this.handleCover.bind(this, 'on'), onMouseLeave: this.handleCover.bind(this, 'off')}, 
                React.createElement("div", {className: "match_pp", style: {background: 'url(' + this.state.profile_prez + ')', backgroundSize: 'cover'}}), 
                React.createElement("div", {id: "t_profile" + this.state.id_matched, className: "n_profile"}, this.props.user_matched), 
                React.createElement("div", {id: "match_button" + this.state.id_matched, style: {display: "none"}}, 
                    React.createElement("img", {className: "m_profile", src: "../../resources/message.png", onClick: this.createConversation, style: {color: "grey"}}), 
                    React.createElement("a", {href: profile_link}, React.createElement("img", {className: "l_profile", src: "../../resources/log.png", style: {color: "grey"}}))
                )
            )
        );
    }

    render()
    {
        return (
            React.createElement("div", null, 
                this.displayMatch()
            )
        );
    }
}

if (typeof module != 'undefined') {
    module.exports = Matches;
} else {
    window.Matches = Matches;
}
