var React = window.React ? window.React : require('react');
var axios = require('axios');
var Tags  = require('react-tag-autocomplete');
var socket = io.connect('http://matcha.bbichero.com:8080');

class Public_profile extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            url_path:       this.props.url_path,
            resources_path: this.props.resources_path,
            item_edit_list: this.props.item_edit_list,
            latitude:       this.props.lat,
            longitude:      this.props.lon,
            notification:   true,
            state:          null,
            piture:         false,
            import_img:     false,
            display_div:    false,
            pp_clicked:     false,
            clicked:        false,
            clicked_1:      false,
            clicked_2:      false,
            test:           false,
            marker:         null,
            current_index:  null,
            current_state:  null,
            status:         this.props.status,
            matchs:         this.props.matchs,
            token:          null,
            state_like:     "like",
            state_dislike:  "dislike",
            tags:           this.props.tags,
            suggestions:    (this.props.suggestions) ? Object.keys(this.props.suggestions).map((key) => this.props.suggestions[key]) : [{ id: 1, name: "Kaplas" }, { id: 2, name: "Mangos" }, { id: 5, name: "Lemons" }, { id: 6, name: "Apricots" }],
        };

        this.state.item_edit_list.forEach((element, index) =>
        {
            this.state.item_edit_list[index].edit = false;
            this.state.item_edit_list[index].new_value = this.state.item_edit_list[index].value;
        });
        this.handleState               = this.handleState.bind(this);
        this.handleUserState           = this.handleUserState.bind(this);
        this.handleReceiveNotification = this.handleReceiveNotification.bind(this);

        socket.emit('connection_user',   this.props.src_username);
        socket.emit('connected_on',      {dst_username: this.props.dst_username, src_username: this.props.src_username, id_dst_username: this.props.id_dst_username, id_src_username: this.props.id_src_username, prez: this.props.src_prez, blocked_by: this.props.blocked_by});
        socket.emit('connection_status', {dst_username: this.props.dst_username, src_username: this.props.src_username, id_dst_username: this.props.id_dst_username, id_src_username: this.props.id_src_username});
    }

    componentDidMount()
    {
        socket.on('status', this.handleUserState);
        socket.on('notification', this.handleReceiveNotification);
    }

    handleUserState(type, data, event)
    {
        if (type === "date")
        {
            var date = new Date(data).toUTCString();
            this.setState({state : date});
        }
        else if (type === "online")
            this.setState({state : " " + data});
    }

    handleReceiveNotification(type, message, event)
    {
        this.setState({notification : null});

        // Get a notification when an user sends you a message.
        if (type === "message")
        {
            this.state.notification =
            React.createElement("div", {id: "notification", className: "notification_container animated fadeIn"}, 
                React.createElement("div", {className: "notification base"}, 
                    React.createElement("div", {className: "base"}, React.createElement("img", {className: "notification_pp base", src: message.prez ? message.prez : "resources/empty_profile.png"})), 
                    React.createElement("span", {className: "notification-close base"}, "✕"), 
                    React.createElement("h3", {className: "notification-title base"}, message.src_username, " sent you a message !"), 
                    React.createElement("p", {className: "notification-message base"}, message.body)
                )
            );
            this.setState({notification : this.state.notification});

            var temp = document.getElementById("notification");
            setTimeout(function(){temp.className += " fadeOut";}, 4000);
        }
        // Get a notification when an user likes your profile.
        if (type === "like")
        {
            this.state.notification =
            React.createElement("div", {id: "notification", className: "notification_container animated fadeIn"}, 
                React.createElement("div", {className: "notification base"}, 
                    React.createElement("div", {className: "base"}, React.createElement("img", {className: "notification_pp base", src: message.prez ? message.prez : "resources/empty_profile.png"})), 
                    React.createElement("span", {className: "notification-close base"}, "✕"), 
                    React.createElement("h3", {className: "notification-title base"}, " Congratulations !"), 
                    React.createElement("p", {className: "notification-message base"}, message.src_username, " liked you !")
                )
            );
            this.setState({notification : this.state.notification});

            var temp = document.getElementById("notification");
            setTimeout(function(){temp.className += " fadeOut";}, 4000);
        }
        // Get a notification when an user match with you by liking your profile.
        if (type === "match")
        {
            this.state.notification =
            React.createElement("div", {id: "notification", className: "notification_container animated fadeIn"}, 
                React.createElement("div", {className: "notification base"}, 
                    React.createElement("div", {className: "base"}, React.createElement("img", {className: "notification_pp base", src: message.prez ? message.prez : "resources/empty_profile.png"})), 
                    React.createElement("span", {className: "notification-close base"}, "✕"), 
                    React.createElement("h3", {className: "notification-title base"}, " Congratulations !"), 
                    React.createElement("p", {className: "notification-message base"}, "You've matched with you ", message.src_username, " !")
                )
            );
            this.setState({notification : this.state.notification});

            var temp = document.getElementById("notification");
            setTimeout(function(){temp.className += " fadeOut";}, 4000);
        }
        // Get a notification when a matched user unlikes you.
        if (type === "unmatch")
        {
            this.state.notification =
            React.createElement("div", {id: "notification", className: "notification_container animated fadeIn"}, 
                React.createElement("div", {className: "notification base"}, 
                    React.createElement("div", {className: "base"}, React.createElement("img", {className: "notification_pp base", src: message.prez ? message.prez : "resources/empty_profile.png"})), 
                    React.createElement("span", {className: "notification-close base"}, "✕"), 
                    React.createElement("h3", {className: "notification-title base"}, " Damn Daniel, match lost !"), 
                    React.createElement("p", {className: "notification-message base"}, message.src_username, " unlikes you !")
                )
            );
            this.setState({notification : this.state.notification});

            var temp = document.getElementById("notification");
            setTimeout(function(){temp.className += " fadeOut";}, 4000);
        }
        // Get a notification when an user is on your profile page.
        if (type === "connected_on")
        {
            this.state.notification =
            React.createElement("div", {id: "notification", className: "notification_container animated fadeIn"}, 
                React.createElement("div", {className: "notification base"}, 
                    React.createElement("div", {className: "base"}, React.createElement("img", {className: "notification_pp base", src: message.prez ? message.prez : "resources/empty_profile.png"})), 
                    /* <span className="notification-close base">&#x2715;</span> */
                    React.createElement("h3", {className: "notification-title base"}, " Hey !"), 
                    React.createElement("p", {className: "notification-message base"}, message.src_username, " is watching your profile !")
                )
            );
            this.setState({notification : this.state.notification});

            var temp = document.getElementById("notification");
            setTimeout(function(){temp.className += " fadeOut";}, 4000);
        }
    }

    displayInfo(class_name, data_type, field_type, radio_fields)
    {
        let index = this.state.item_edit_list.findIndex(x => x.name == data_type);
        return (
            React.createElement("div", null, 
                React.createElement("div", {className: class_name}, this.state.item_edit_list[index].value)
            )
        );
    }

    handleFieldType(index, data_type, field_type, select_fields = null)
    {
        if (field_type === "text")
            return (
                React.createElement("input", {className: "content", type: "text", 
                        value: this.state.item_edit_list[index].new_value, 
                        onChange: this.handleEditChange.bind(this, index)}
                ));
        else if (field_type === "radio")
        {
            const radios = select_fields.map((field, key) =>
            {
                if (field === this.state.item_edit_list[index].value)
                    return (React.createElement("input", {type: "radio", name: data_type, 
                                    value: field, checked: true, 
                                    onChange: this.handleEditChange.bind(this, index)}));
                else
                    return (React.createElement("input", {type: "radio", name: data_type, 
                                    value: field, 
                                    onChange: this.handleEditChange.bind(this, index)}));
            });
            return radios;
        }
        else if (field_type === "email")
            return (
                React.createElement("input", {className: "content", type: "email", 
                        value: this.state.item_edit_list[index].new_value, 
                        onChange: this.handleEditChange.bind(this, index)}
                ));
        else if (field_type === "select")
        {
            const select = select_fields.map((field, key) =>
            {
                    return (React.createElement("option", {key: key}, field));
            });
            return (
                React.createElement("select", {name: data_type, width: "40px", 
                        value: this.state.item_edit_list[index].new_value, 
                        onChange: this.handleEditChange.bind(this, index)}, 
                        React.createElement("optgroup", {label: "Choose a value"}, "Option Group "), 
                        select
                )

            );
        }
    }

    handleMorsay(data_type, index, event)
    {
        if (this.state.item_edit_list[index].edit === false)
            this.state.item_edit_list[index].edit = true;
        else
            this.state.item_edit_list[index].edit = false;

        this.setState({ state_array: this.state.item_edit_list });
    }

    handleDelete(i)
    {
        alert ("By Odin, you cannot delete the others' tags !");
    }

    handleAddition(tag)
    {
        alert ("No addition is athorized when you are on others' profile !");
    }

    displayTags()
    {
        if (this.state.tags != null)
        {
            const listTags = this.state.tags.map((number, key) =>
            { return ( React.createElement("li", {className: "react-tags__selected-tag", key: key}, number.name) ); });
            return(React.createElement("div", {className: "content"}, listTags));
        }
    }


    handlePicture_0()
    {
        this.setState({clicked: true});
        this.state.clicked = true;
    }

    handlePicture_1()
    {
        this.setState({clicked_1: true});
        this.state.clicked_1 = true;
    }

    handlePicture_2()
    {
        this.setState({clicked_2: true});
        this.state.clicked_2 = true;
    }

    handleDisablePicture()
    {
        this.setState({clicked: false});
        this.state.clicked = false;
        this.setState({clicked_1: false});
        this.state.clicked_1 = false;
        this.setState({clicked_2: false});
        this.state.clicked_2 = false;
    }

    handleBlockRequest()
    {
        var confirm = window.confirm('Do you really want to block this user ?');
        if (confirm)
        {
            let value = 'type=set_block&username=' + this.props.dst_username;
            axios(
            {
                method: 'POST',
                url: 'http://correction.matcha.bbichero.com/post.php',
                data: value
            }).then((response) =>
            {
                if (response.data === "already_blocked")
                    alert("User already blocked !");
            });
        }
        return confirm;
    }

    handleRelationState(state, event)
    {
        if (state === "like" && this.props.active == 'Y')
        {
            let value = 'type=relation_state&state=' + state + '&id_liked=' + this.props.id;
            axios(
            {
                method: 'POST',
                url: 'http://matcha.bbichero.com/post.php',
                data: value
            }).then((response) =>
            {
                if (response.data === "already_liked")
                    alert("User already liked !");
                if (response.data === "type=relation_state&id_liked=" + this.props.id)
                {
                    this.setState({current_state : "Liked User"});
                    socket.emit('like', {dst_username: this.props.dst_username, src_username: this.props.src_username, id_dst_username: this.props.id_dst_username, id_src_username: this.props.id_src_username, prez: this.props.src_prez, blocked_by: this.props.blocked_by});
                }
                if (response.data === "matched")
                {
                    this.setState({current_state : "Matched User"});
                    socket.emit('match', {dst_username: this.props.dst_username, src_username: this.props.src_username, id_dst_username: this.props.id_dst_username, id_src_username: this.props.id_src_username, prez: this.props.src_prez, blocked_by: this.props.blocked_by});
                    alert("You have matched with " + this.props.dst_username);
                }

            });
        }
        if (state === "dislike" && this.props.active == 'Y')
        {
            let value = 'type=relation_state&state=' + state + '&id_liked=' + this.props.id;
            axios(
            {
                method: 'POST',
                url: 'http://matcha.bbichero.com/post.php',
                data: value
            }).then((response) =>
            {
                if (response.data === "not_liked")
                    alert("You cannot unlike a user not liked!");
                if (response.data === "type=relation_state&id_liked=" + this.props.id)
                    this.setState({current_state : ""});
                if (this.state.status && this.state.matchs === false)
                    this.setState({current_state : this.props.dst_username + " liked you"});
                if (response.data === "unmatched")
                {
                    socket.emit('unmatch', {dst_username: this.props.dst_username, src_username: this.props.src_username, id_dst_username: this.props.id_dst_username, id_src_username: this.props.id_src_username, prez: this.props.src_prez, blocked_by: this.props.blocked_by});
                    alert("You unmatched with " + this.props.dst_username);
                    this.setState({current_state : this.props.dst_username + " liked you"});
                }
            });
        }
        else if (this.props.active != 'Y')
            alert("First you need to set a profile picture before interract with the community !");
    }

    handleState()
    {
        if (this.state.status && this.state.matchs === false)
            return (React.createElement("div", {style: {display: "inline-block"}}, this.props.dst_username, " liked you "));
        else if (this.state.status && this.state.matchs)
            return (React.createElement("div", {style: {display: "inline-block"}}, "Matched User"));
    }

// *****************************************************
// Render of the page: Render of the User Settings page.
// *****************************************************

    render()
    {
        const { tags, suggestions } = this.state;
        return (
            React.createElement("div", {className: "pure-form", style: {width: "100%", height: "100%"}}, 
                React.createElement("div", {className: "public_top_div center"}, 
                    React.createElement("img", {onClick: this.handlePicture_1.bind(this, this.state.clicked_1), className: (this.state.clicked_1 === true) ? "public_profile_big" : "alt_picture_l", style: this.state.clicked_2 || this.state.clicked   ? {display: "none"} : {display: ""}, src: (this.props.alt1) ? this.props.alt1 : "../../resources/empty_profile.png"}), 
                    React.createElement("img", {onClick: this.handlePicture_2.bind(this, this.state.clicked_2), className: (this.state.clicked_2 === true) ? "public_profile_big" : "alt_picture_r", style: this.state.clicked_1 || this.state.clicked   ? {display: "none"} : {display: ""}, src: (this.props.alt2) ? this.props.alt2 : "../../resources/empty_profile.png"}), 
                    React.createElement("img", {onClick: this.handlePicture_0.bind(this, this.state.clicked), className: (this.state.clicked   === true) ? "public_profile_big" : "user_picture ", style: this.state.clicked_2 || this.state.clicked_1 ? {display: "none"} : {display: ""}, src: (this.props.prez) ? this.props.prez : "../../resources/empty_profile.png"}), 
                    React.createElement("div", {style: this.state.clicked_2 || this.state.clicked_1 || this.state.clicked ? {display: "none"} : {display: ""}}, this.props.dst_username, " - Level: ", this.props.level)
                ), 
                React.createElement("div", {className: "public_top_match center blocks"}, 
                    React.createElement("img", {className: "nope_public", src: "../../resources/nope.png", onClick: this.handleRelationState.bind(this, this.state.state_dislike)}), 
                    React.createElement("div", {style: {display: "inline-block"}}, (this.state.current_state != null) ? this.state.current_state : this.handleState(), " -"), 
                    this.state.state, 

                    React.createElement("img", {className: "yope_public", src: "../../resources/yope.png", onClick: this.handleRelationState.bind(this, this.state.state_like)})
                ), 
                React.createElement("div", {className: "public_middle blocks"}, 
                    React.createElement("div", {className: "title_settings_blocks"}, "Informations"), 
                    React.createElement("div", {className: "block_user", onClick: this.handleBlockRequest.bind(this)}, "Block user"), 
                    React.createElement("div", {className: "bio"}, 
                        React.createElement("div", {className: "subtitle_settings_blocks"}, "Bio: "), 
                        this.displayInfo("content", "bio", "text")
                    ), 
                    React.createElement("div", {className: "email"}, 
                        React.createElement("div", {className: "subtitle_settings_blocks"}, "Age:"), 
                        this.displayInfo("content", "age", "text")
                    ), 
                    React.createElement("div", {className: "gender_chan"}, 
                        React.createElement("div", {className: "subtitle_settings_blocks"}, "Gender: "), 
                        this.displayInfo("content", "gender", "select", Array("Male", "Female"))
                    ), 
                    React.createElement("div", {className: "orientation_chan"}, 
                        React.createElement("div", {className: "subtitle_settings_blocks"}, "Orientation: "), 
                        this.displayInfo("content", "orientation", "select", Array("H", "B", "T"))
                    ), 
                    React.createElement("div", {className: "phone"}, 
                        React.createElement("div", {className: "subtitle_settings_blocks"}, "Phone number:"), 
                        this.displayInfo("content", "phone", "text")
                    ), 
                    React.createElement("div", {className: "city"}, 
                        React.createElement("div", {className: "subtitle_settings_blocks"}, "City: "), 
                        this.displayInfo("content", "city", "text")
                    ), 
                    React.createElement("div", {className: "tags"}, 
                        React.createElement("div", {className: "subtitle_settings_blocks"}, "Tags:"), 
                        this.displayTags()
                    )
                ), 
                React.createElement("div", {onClick: this.handleDisablePicture.bind(this), id: (this.state.clicked === true || this.state.clicked_1 === true || this.state.clicked_2 === true) ? "cover" : ""}), 
                React.createElement("div", {id: "footer"}, this.state.notification)
            )
        );
    }
}


// *************************
// End: Class Public_profile.
// *************************

if (typeof module != 'undefined')
{
    module.exports = Public_profile;
}
else
{
    window.Public_profile = Public_profile;
}


// **********
// END: File.
// **********
