var React = window.React ? window.React : require('react');
var socket = io.connect('http://matcha.bbichero.com:8080');
var axios = require('axios');

class People_near extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            profiles_list_base: this.props.profiles_near.slice(0),
            profiles_list: null,
            active_profile: null,
            advance_search: true,
            notification: true,
            selected_arr : {
                age_min:  0,
                age_max:  99,
                lvl_min:  0,
                lvl_max:  99,
                tag_min:  0,
                tag_max:  99,
                dist_min: 0,
                dist_max: 99,
            },
            search_arr : {
                age_min:  0,
                age_max:  99,
                lvl_min:  0,
                lvl_max:  99,
                tag_min:  0,
                tag_max:  99,
                dist_min: 0,
                dist_max: 99,
            },
        };

        this.state.profiles_list_base.sort(function(a, b) {if (!b.dist || !a.dist) return 0; return b.nb_tags - a.nb_tags;});
        this.state.profiles_list_base.sort(function(a, b) {if (!b.dist || !a.dist) return 0; return b.lvl - a.lvl;});
        this.state.profiles_list_base.sort(function(a, b) {return a.dist - b.dist;});

        this.state.profiles_list = this.state.profiles_list_base.slice(0);

        // Set profiles states
        this.state.profiles_list.forEach((element, index) =>
        {
            this.state.profiles_list[index].mouseOn = false;
        });

        this.onChangeSort              = this.onChangeSort.bind(this);
        this.displayProfiles           = this.displayProfiles.bind(this);
        this.handleSearchBar           = this.handleSearchBar.bind(this);
        this.handleSelectSort          = this.handleSelectSort.bind(this);
        this.handleResetFilter         = this.handleResetFilter.bind(this);
        // this.handleClickReport         = this.handleClickReport.bind(this);
        this.handleSelectFilter        = this.handleSelectFilter.bind(this);
        this.handleSubmitFilter        = this.handleSubmitFilter.bind(this);
        this.handleReceiveNotification = this.handleReceiveNotification.bind(this);

        socket.emit('connection_user', this.props.src_username);
    }

    componentDidMount()
    {
        socket.on('notification', this.handleReceiveNotification);

        for (var cpt = 0; cpt <= 15; cpt++) {
            var min = 0, max = 99, select = document.getElementById('select' + cpt);
            for (var i = min; i <= max; i++) {
                var opt = document.createElement('option');
                opt.value = i;
                opt.innerHTML = i;
                select.appendChild(opt); } }
        this.setState({selected_arr: this.state.selected_arr});
    }



// ****************************************************************** //
// This is all the notifications event handler needed to display to   //
// the user the different notifications cases  .                      //
// ****************************************************************** //


    handleReceiveNotification(type, message, event)
    {
        this.setState({notification : null});

        // Get a notification when an user sends you a message.
        if (type === "message")
        {
            this.state.notification =
            React.createElement("div", {id: "notification", className: "notification_container animated fadeIn"}, 
                React.createElement("div", {className: "notification base"}, 
                    React.createElement("div", {className: "base"}, React.createElement("img", {className: "notification_pp base", src: message.prez ? message.prez : "resources/empty_profile.png"})), 
                    React.createElement("h3", {className: "notification-title base"}, message.src_username, " sent you a message !"), 
                    React.createElement("p", {className: "notification-message base"}, message.body)
                )
            );
            this.setState({notification : this.state.notification});

            var temp = document.getElementById("notification");
            setTimeout(function(){temp.className += " fadeOut";}, 4000);
        }
        // Get a notification when an user likes your profile.
        if (type === "like")
        {
            this.state.notification =
            React.createElement("div", {id: "notification", className: "notification_container animated fadeIn"}, 
                React.createElement("div", {className: "notification base"}, 
                    React.createElement("div", {className: "base"}, React.createElement("img", {className: "notification_pp base", src: message.prez ? message.prez : "resources/empty_profile.png"})), 
                    React.createElement("h3", {className: "notification-title base"}, " Congratulations !"), 
                    React.createElement("p", {className: "notification-message base"}, message.src_username, " liked you !")
                )
            );
            this.setState({notification : this.state.notification});

            var temp = document.getElementById("notification");
            setTimeout(function(){temp.className += " fadeOut";}, 4000);
        }
        // Get a notification when an user match with you by liking your profile.
        if (type === "match")
        {
            this.state.notification =
            React.createElement("div", {id: "notification", className: "notification_container animated fadeIn"}, 
                React.createElement("div", {className: "notification base"}, 
                    React.createElement("div", {className: "base"}, React.createElement("img", {className: "notification_pp base", src: message.prez ? message.prez : "resources/empty_profile.png"})), 
                    React.createElement("h3", {className: "notification-title base"}, " Congratulations !"), 
                    React.createElement("p", {className: "notification-message base"}, "You've matched with you ", message.src_username, " !")
                )
            );
            this.setState({notification : this.state.notification});

            var temp = document.getElementById("notification");
            setTimeout(function(){temp.className += " fadeOut";}, 4000);
        }
        // Get a notification when a matched user unlikes you.
        if (type === "unmatch")
        {
            this.state.notification =
            React.createElement("div", {id: "notification", className: "notification_container animated fadeIn"}, 
                React.createElement("div", {className: "notification base"}, 
                    React.createElement("div", {className: "base"}, React.createElement("img", {className: "notification_pp base", src: message.prez ? message.prez : "resources/empty_profile.png"})), 
                    React.createElement("h3", {className: "notification-title base"}, " Damn Daniel, match lost !"), 
                    React.createElement("p", {className: "notification-message base"}, message.src_username, " unlikes you !")
                )
            );
            this.setState({notification : this.state.notification});

            var temp = document.getElementById("notification");
            setTimeout(function(){temp.className += " fadeOut";}, 4000);
        }
        // Get a notification when an user is on your profile page.
        if (type === "connected_on")
        {
            this.state.notification =
            React.createElement("div", {id: "notification", className: "notification_container animated fadeIn"}, 
                React.createElement("div", {className: "notification base"}, 
                    React.createElement("div", {className: "base"}, React.createElement("img", {className: "notification_pp base", src: message.prez ? message.prez : "resources/empty_profile.png"})), 
                    React.createElement("h3", {className: "notification-title base"}, " Hey !"), 
                    React.createElement("p", {className: "notification-message base"}, message.src_username, " is watching your profile !")
                )
            );
            this.setState({notification : this.state.notification});

            var temp = document.getElementById("notification");
            setTimeout(function(){temp.className += " fadeOut";}, 4000);
        }
    }



// ****************************************************************** //
// This is all the tools function needed to activate and disable the  //
// selected profile and use the report function.                      //
// ****************************************************************** //


    handleActiveProfile(profile, event)
    {
        this.setState({active_profile: profile});
        let index = this.state.profiles_list.indexOf(profile);
        this.state.profiles_list[index].mouseOn = true;
    }

    handleDisableProfile(profile, event)
    {
        this.setState({active_profile: profile});
        let index = this.state.profiles_list.indexOf(profile);
        this.state.profiles_list[index].mouseOn = false;
    }

    handleClickReport(profile)
    {
        let value = 'type=set_report&username=' + profile.username;
        axios(
        {
            method: 'POST',
            url: 'http://matcha.bbichero.com/post.php',
            data: value
        }).then((response) =>
        {
            if (response.data === 0)
                alert("User already reported !");
            else
            {
                var temp = document.getElementById("tempMessage");
                temp.style.display = "";
                setTimeout(function()
                {
                    temp.className += " animated fadeOutDown";
                }, 2000);
            }
        });
    }



// ******************************************************************* //
// This is all the display function fot the differents elements on     //
// the client render. You can find the genral display and his intern   //
// displays.                                                           //
// ******************************************************************* //


    displayCover(profile)
    {
        if (profile.mouseOn === true)
            return (React.createElement("div", {id: "cover"}));
    }
    displayMatchSystem(profile)
    {
        if (profile.mouseOn === true)
            return (React.createElement("div", {className: "match_system"}));
    }
    handleDisplayTiny(profile)
    {
        if (profile.mouseOn === false)
            return (
                React.createElement("div", null, 
                    React.createElement("div", null, profile.username), 
                    React.createElement("div", null, profile.age, " yo"), 
                    React.createElement("div", null, profile.gender), 
                    React.createElement("div", {style: (profile.dist || profile.dist === 0) ? {display: ""} : {display: "none"}}, " Near ", profile.dist, "km"), 
                    React.createElement("div", null, profile.nb_tags, " - ", profile.lvl)
                ));
    }
    handleDisplayMenu(profile)
    {
        if (profile.mouseOn === true)
        {
            var public_profile = "../../public_profile.php?profile_username=" + profile.username;
            return (
                React.createElement("div", {className: "animated slideInUp menu"}, 
                    React.createElement("div", {className: "bio_des"}, "Bio:", profile.bio), 
                    React.createElement("div", {className: "report", onClick: this.handleClickReport.bind(this, profile)}, "Report as fake"), 
                    React.createElement("div", {className: "center"}, React.createElement("a", {href: this.state.url_path + public_profile}, "Go to ", profile.username, "'s profile")), 
                    React.createElement("div", {className: "animated fadeInDown", id: "tempMessage", style: {display: "none"}}, "The user has been successfully reported !")
                ));
        }
    }
    displayProfiles(profiles_list)
    {

        const profiles = profiles_list.map((profile, index) => {
            return (
                React.createElement("div", {key: index}, 
                    React.createElement("div", {onClick: this.handleActiveProfile.bind(this, profile)}, 
                        React.createElement("div", {className: (profile.mouseOn === true) ? "profiles_div_big" : "profiles_div", 
                              style:  (profile.prez) ? {width: 'null', height: 'null', background: 'url(' + profile.prez + ')', backgroundSize: 'cover'} : {}}, 

                            this.handleDisplayTiny(profile), 
                            this.handleDisplayMenu(profile)
                        )
                    ), 
                    React.createElement("div", {onClick: this.handleDisableProfile.bind(this, profile), id: (profile.mouseOn === true) ? "cover" : ""})
                )
            );
        });
        return profiles;
    }


// ******************************************************************* //
// This is the sort part who brings an adaptive sorted list of the     //
// current render. You can find the display div of the feature and     //
// all the tools needed for the sort.                                  //
// ******************************************************************* //

    onChangeSort(event)
    {
        if (event.target.value == "age")
            this.state.profiles_list.sort(function(a, b) { return a.age - b.age;});
        else if (event.target.value == "lvl")
            this.state.profiles_list.sort(function(a, b) { return b.lvl - a.lvl;});
        else if (event.target.value == "tags")
            this.state.profiles_list.sort(function(a, b) { return b.nb_tags - a.nb_tags;});
        else if (event.target.value == "dist")
            this.state.profiles_list.sort(function(a, b) { if (!a.dist && a.dist != 0) {return 1;} if (!b.dist && b.dist != 0) {return -1;} return a.dist - b.dist;});
        this.setState({profiles_list: this.state.profiles_list});
    }

    handleSelectSort(event)
    {
        return (
            React.createElement("div", null, 
                React.createElement("select", {defaultValue: "Sort by", onChange: this.onChangeSort}, 
                    React.createElement("option", {value: "Sort by", disabled: true}, "Sort by"), 
                    React.createElement("option", {value: "age"}, "age"), 
                    React.createElement("option", {value: "lvl"}, "lvl"), 
                    React.createElement("option", {value: "tags"}, "tags"), 
                    React.createElement("option", {value: "dist"}, "dist")
                )
            )
        );
    }


// ******************************************************************* //
// This is the filter part who brings an adaptive result by the user   //
// parameters. You can find the display div of the feature and all     //
// the tools needed for the filtering.                                 //
// ******************************************************************* //

    onChangeFilter(selected, event)
    {
        this.state.selected_arr[selected] = event.target.value;
        this.setState({selected_arr: this.state.selected_arr});
    }

    handleResetFilter(event)
    {
        for (var key in this.state.selected_arr)
        {
            if (key[key.length - 1] == "x")
                this.state.selected_arr[key] = 99;
            else if (key[key.length - 1] == "n")
                this.state.selected_arr[key] = 0;
        }
        this.setState({selected_arr: this.state.selected_arr});
        this.setState({profiles_list: this.state.profiles_list_base});
    }

    handleFiltering(tmp_list_or, min, max, type)
    {
        var type_num = null;
        var tmp_list = tmp_list_or.slice(0);
        for (var key = 0; tmp_list[key]; key++)
        {
            if (min >= max)
                break;
            else {
                if (type == "age")
                    type_num = tmp_list[key].age;
                if (type == "lvl")
                    type_num = tmp_list[key].lvl;
                if (type == "tag")
                    type_num = tmp_list[key].nb_tags;
                if (type == "dist")
                    type_num = tmp_list[key].dist;
                if (type == "dist" && typeof tmp_list[key].dist == "number" && !tmp_list[key].dist)
                    type_num = 0;
                if (type == "dist" && typeof tmp_list[key].dist != "number" && !tmp_list[key].dist)
                    type_num = null;
                if (type_num >= min && type_num <= max)
                    // console.log(type_num)
                    ;
                else {
                    if(type == "dist")
                    {
                        // console.log(type_num);
                        // console.log(tmp_list[key]);
                        ;
                    }
                    var index = tmp_list.indexOf(tmp_list[key]);
                    if (index > -1) {
                        key = -1;
                        tmp_list.splice(index, 1);
                    }
                }
            }
        }
        return (tmp_list);
    }

    handleSubmitFilter(event)
    {
        this.state.profiles_list = this.state.profiles_list_base;
        const age_min  = parseInt(this.state.selected_arr.age_min, 10);  const age_max  = parseInt(this.state.selected_arr.age_max, 10);
        const lvl_min  = parseInt(this.state.selected_arr.lvl_min, 10);  const lvl_max  = parseInt(this.state.selected_arr.lvl_max, 10);
        const tag_min  = parseInt(this.state.selected_arr.tag_min, 10);  const tag_max  = parseInt(this.state.selected_arr.tag_max, 10);
        const dist_min = parseInt(this.state.selected_arr.dist_min, 10); const dist_max = parseInt(this.state.selected_arr.dist_max, 10);
        var tmp_list = this.state.profiles_list;
        tmp_list = this.handleFiltering(tmp_list, age_min, age_max, "age");
        tmp_list = this.handleFiltering(tmp_list, lvl_min, lvl_max, "lvl");
        tmp_list = this.handleFiltering(tmp_list, tag_min, tag_max, "tag");
        tmp_list = this.handleFiltering(tmp_list, dist_min, dist_max, "dist");
        if (age_min < age_max && lvl_min < lvl_max && tag_min < tag_max && dist_min < dist_max)
            this.setState({profiles_list: tmp_list});
        else
        {
            alert("Please enter correct values in your comparaison, left must be inferior.");
        }
    }

    handleSelectFilter(event)
    {
        return (
            React.createElement("div", null, 
                React.createElement("div", {className: "filter_box"}, 
                    /* Age Range */
                    React.createElement("label", {className: "search-label"}, "Age between : "), 
                    React.createElement("select", {id: "select0", onChange: this.onChangeFilter.bind(this, "age_min"), value: this.state.selected_arr.age_min}), 
                    React.createElement("label", {className: "search-label"}, " and "), 
                    React.createElement("select", {id: "select1", onChange: this.onChangeFilter.bind(this, "age_max"), value: this.state.selected_arr.age_max}), " ", React.createElement("br", null), 
                    /* Level Range */
                    React.createElement("label", {className: "search-label"}, "Level between: "), 
                    React.createElement("select", {id: "select2", onChange: this.onChangeFilter.bind(this, "lvl_min"), value: this.state.selected_arr.lvl_min}), 
                    React.createElement("label", {className: "search-label"}, " and "), 
                    React.createElement("select", {id: "select3", onChange: this.onChangeFilter.bind(this, "lvl_max"), value: this.state.selected_arr.lvl_max}), " ", React.createElement("br", null), 
                    /* Common Tags  */
                    React.createElement("label", {className: "search-label"}, "Common tags between: "), 
                    React.createElement("select", {id: "select4", onChange: this.onChangeFilter.bind(this, "tag_min"), value: this.state.selected_arr.tag_min}), 
                    React.createElement("label", {className: "search-label"}, " and "), 
                    React.createElement("select", {id: "select5", onChange: this.onChangeFilter.bind(this, "tag_max"), value: this.state.selected_arr.tag_max}), " ", React.createElement("br", null), 
                    /* Distance  */
                    React.createElement("label", {className: "search-label"}, "Distance between: "), 
                    React.createElement("select", {id: "select6", onChange: this.onChangeFilter.bind(this, "dist_min"), value: this.state.selected_arr.dist_min}), 
                    React.createElement("label", {className: "search-label"}, " and "), 
                    React.createElement("select", {id: "select7", onChange: this.onChangeFilter.bind(this, "dist_max"), value: this.state.selected_arr.dist_max}), 
                    React.createElement("br", null), React.createElement("button", {onClick: this.handleResetFilter}, "Reset"), 
                    React.createElement("br", null), React.createElement("button", {onClick: this.handleSubmitFilter}, "Submit")
                )
            )
        );
    }


// ******************************************************************* //
// This is the search part who brings an adaptive list of users by     //
// users parameters. You can find the display div of the feature and   //
// all the tools needed for the search.                                //
// ******************************************************************* //

    onChangeSearch(search, event)
    {
        this.state.search_arr[search] = event.target.value;
        this.setState({search_arr: this.state.search_arr});
    }

    handleSearchBar(profile)
    {
        return(
        React.createElement("div", null, React.createElement("br", null), 
            React.createElement("form", {name: "frmSearch", method: "post", action: "index.php"}, 
                React.createElement("input", {type: "hidden", id: "advance_search_submit", name: "advance_search_submit", value: ""}), 
                React.createElement("div", {className: "search_box"}, 
                    /* <div>Search:    </div><br></br><br></br> */
                    React.createElement("div", {id: "advanced-search-box", style: (this.state.advance_search) ? {display: ""} : {display: ""}}, 
                        React.createElement("label", {className: "search-label"}, "Gender: "), 
                        React.createElement("select", {name: "gender", defaultValue: "Choose"}, 
                            React.createElement("option", {value: "Choose", disabled: true}, "Choose"), 
                            React.createElement("option", {value: "male"}, "Male"), 
                            React.createElement("option", {value: "female"}, "Female"), 
                            React.createElement("option", {value: "other"}, "Other")
                        ), 
                        React.createElement("label", {className: "search-label"}, " Orientation: "), 
                        React.createElement("select", {name: "orientation", defaultValue: "Choose"}, 
                            React.createElement("option", {value: "Choose", disabled: true}, "Choose"), 
                            React.createElement("option", {value: "straight"}, "Straight"), 
                            React.createElement("option", {value: "gay"}, "Gay"), 
                            React.createElement("option", {value: "bisexual"}, "Bisexual")
                        ), " ", React.createElement("br", null), 
                        /* Age Range */
                        React.createElement("label", {className: "search-label"}, "Age between      : "), 
                        React.createElement("select", {id: "select8", name: "age_min", onChange: this.onChangeSearch.bind(this, "age_min"), value: this.state.search_arr.age_min}), 
                        React.createElement("label", {className: "search-label"}, " and "), 
                        React.createElement("select", {id: "select9", name: "age_max", onChange: this.onChangeSearch.bind(this, "age_max"), value: this.state.search_arr.age_max}), " ", React.createElement("br", null), 
                        /* Level Range */
                        React.createElement("label", {className: "search-label"}, "Level between: "), 
                        React.createElement("select", {id: "select10", name: "lvl_min", onChange: this.onChangeSearch.bind(this, "lvl_min"), value: this.state.search_arr.lvl_min}), 
                        React.createElement("label", {className: "search-label"}, " and "), 
                        React.createElement("select", {id: "select11", name: "lvl_max", onChange: this.onChangeSearch.bind(this, "lvl_max"), value: this.state.search_arr.lvl_max}), " ", React.createElement("br", null), 
                        /* Common Tags  */
                        React.createElement("label", {className: "search-label"}, "Common tags between: "), 
                        React.createElement("select", {id: "select12", name: "tag_min", onChange: this.onChangeSearch.bind(this, "tag_min"), value: this.state.search_arr.tag_min}), 
                        React.createElement("label", {className: "search-label"}, " and "), 
                        React.createElement("select", {id: "select13", name: "tag_max", onChange: this.onChangeSearch.bind(this, "tag_max"), value: this.state.search_arr.tag_max}), " ", React.createElement("br", null), 
                        /* Distance  */
                        React.createElement("label", {className: "search-label"}, "Distance between: "), 
                        React.createElement("select", {id: "select14", name: "dist_min", onChange: this.onChangeSearch.bind(this, "dist_min"), value: this.state.search_arr.dist_min}), 
                        React.createElement("label", {className: "search-label"}, " and "), 
                        React.createElement("select", {id: "select15", name: "dist_max", onChange: this.onChangeSearch.bind(this, "dist_max"), value: this.state.search_arr.dist_max}), " ", React.createElement("br", null), React.createElement("br", null), 
                        React.createElement("div", null, 
                            React.createElement("input", {className: "center", type: "submit", name: "search", className: "btnSearch", value: "Search"})
                        )
                    )
                )
            )
        )
        );
    }

// ***************************************************************************** //
// This is the filter part who brings an adaptive result by the user parameters. //
// ***************************************************************************** //

    render()
    {
        return (
            React.createElement("div", null, 
                React.createElement("div", {className: "main_div"}, 
                    React.createElement("div", {className: "rendered_list"}, 
                        this.displayProfiles(this.state.profiles_list)
                    ), 
                    React.createElement("center", null, this.handleSelectSort()), 
                    React.createElement("center", null, this.handleSelectFilter()), 
                    React.createElement("center", null, this.handleSearchBar())
                ), 
                React.createElement("div", {id: "footer"}, this.state.notification)
            )
        );
    }
}

if (typeof module != 'undefined')
{
    module.exports = People_near;
}
else
{
    window.People_near = People_near;
}

// xxxxxxxxxxxx //
// End of page. //
// xxxxxxxxxxxx //
