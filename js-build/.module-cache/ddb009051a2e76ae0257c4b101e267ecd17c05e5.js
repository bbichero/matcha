var React = window.React ? window.React : require('react');
var axios = require('axios');
var Tags  = require('react-tag-autocomplete');
var socket = io.connect('http://correction.matcha.bbichero.com:8080');

class User_info_top extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            url_path:       this.props.url_path,
            resources_path: this.props.resources_path,
            item_edit_list: this.props.item_edit_list,
            latitude:       this.props.lat,
            longitude:      this.props.lon,
            notification:   true,
            piture:         false,
            import_img:     false,
            display_div:    false,
            pp_clicked:     false,
            pp_clicked_l:   false,
            pp_clicked_r:   false,
            marker:         null,
            current_index:  null,
            tags:           (this.props.tags) ? (this.props.tags) : [{ id: 1, name:  "Sample"}],
            suggestions:    (this.props.suggestions) ? Object.keys(this.props.suggestions).map((key) => this.props.suggestions[key]) : [{ id: 1, name: "Kaplas" }, { id: 2, name: "Mangos" }, { id: 5, name: "Lemons" }, { id: 6, name: "Apricots" }],
        };

        this.state.item_edit_list.forEach((element, index) =>
        {
            this.state.item_edit_list[index].edit = false;
            this.state.item_edit_list[index].new_value = this.state.item_edit_list[index].value;
        });

        this.handleCreateDiv = this.handleCreateDiv.bind(this);
        this.handleRemoveMap = this.handleRemoveMap.bind(this);
        this.handleUpload    = this.handleUpload.bind(this);
        this.handleUpload_l  = this.handleUpload_l.bind(this);
        this.handleUpload_r  = this.handleUpload_r.bind(this);
        this.validateCity    = this.validateCity.bind(this);
        this.handleDelete    = this.handleDelete.bind(this);
        this.handleAddition  = this.handleAddition.bind(this);
        this.handleReceiveNotification = this.handleReceiveNotification.bind(this);

        socket.emit('connection_user', this.props.src_username);
    }

    componentDidMount()
    {
        socket.on('notification', this.handleReceiveNotification);
    }

    componentDidUpdate()
    {
        if (this.state.display_div)
            this.handleCreateMap();
    }

    handleReceiveNotification(type, message, event)
    {
        this.setState({notification : null});
        // Get a notification when an user sends you a message.
        if (type === "message")
        {
            this.state.notification =
            React.createElement("div", {id: "notification", className: "notification_container animated fadeIn"}, 
                React.createElement("div", {className: "notification base"}, 
                    React.createElement("div", {className: "base"}, React.createElement("img", {className: "notification_pp base", src: message.prez ? message.prez : "resources/empty_profile.png"})), 
                    React.createElement("h3", {className: "notification-title base"}, message.src_username, " sent you a message !"), 
                    React.createElement("p", {className: "notification-message base"}, message.body)
                )
            );
            this.setState({notification : this.state.notification});

            var temp = document.getElementById("notification");
            setTimeout(function(){temp.className += " fadeOut";}, 4000);
        }
        // Get a notification when an user likes your profile.
        if (type === "like")
        {
            this.state.notification =
            React.createElement("div", {id: "notification", className: "notification_container animated fadeIn"}, 
                React.createElement("div", {className: "notification base"}, 
                    React.createElement("div", {className: "base"}, React.createElement("img", {className: "notification_pp base", src: message.prez ? message.prez : "resources/empty_profile.png"})), 
                    React.createElement("h3", {className: "notification-title base"}, " Congratulations !"), 
                    React.createElement("p", {className: "notification-message base"}, message.src_username, " liked you !")
                )
            );
            this.setState({notification : this.state.notification});

            var temp = document.getElementById("notification");
            setTimeout(function(){temp.className += " fadeOut";}, 4000);
        }
        // Get a notification when an user match with you by liking your profile.
        if (type === "match")
        {
            this.state.notification =
            React.createElement("div", {id: "notification", className: "notification_container animated fadeIn"}, 
                React.createElement("div", {className: "notification base"}, 
                    React.createElement("div", {className: "base"}, React.createElement("img", {className: "notification_pp base", src: message.prez ? message.prez : "resources/empty_profile.png"})), 
                    React.createElement("h3", {className: "notification-title base"}, " Congratulations !"), 
                    React.createElement("p", {className: "notification-message base"}, "You've matched with you ", message.src_username, " !")
                )
            );
            this.setState({notification : this.state.notification});

            var temp = document.getElementById("notification");
            setTimeout(function(){temp.className += " fadeOut";}, 4000);
        }
        // Get a notification when a matched user unlikes you.
        if (type === "unmatch")
        {
            this.state.notification =
            React.createElement("div", {id: "notification", className: "notification_container animated fadeIn"}, 
                React.createElement("div", {className: "notification base"}, 
                    React.createElement("div", {className: "base"}, React.createElement("img", {className: "notification_pp base", src: message.prez ? message.prez : "resources/empty_profile.png"})), 
                    React.createElement("h3", {className: "notification-title base"}, " Damn Daniel, match lost !"), 
                    React.createElement("p", {className: "notification-message base"}, message.src_username, " unlikes you !")
                )
            );
            this.setState({notification : this.state.notification});

            var temp = document.getElementById("notification");
            setTimeout(function(){temp.className += " fadeOut";}, 4000);
        }
        // Get a notification when an user is on your profile page.
        if (type === "connected_on")
        {
            this.state.notification =
            React.createElement("div", {id: "notification", className: "notification_container animated fadeIn"}, 
                React.createElement("div", {className: "notification base"}, 
                    React.createElement("div", {className: "base"}, React.createElement("img", {className: "notification_pp base", src: message.prez ? message.prez : "resources/empty_profile.png"})), 
                    React.createElement("h3", {className: "notification-title base"}, " Hey !"), 
                    React.createElement("p", {className: "notification-message base"}, message.src_username, " is watching your profile !")
                )
            );

            this.setState({notification : this.state.notification});

            var temp = document.getElementById("notification");
            setTimeout(function(){temp.className += " fadeOut";}, 4000);
        }
    }


// *****************************************************************************
// Map feature: Create a map with the Google API who permit to change the user
// location with a marker system validated with a check button, cancel in an
// other hand. The feature update coordinates and the city in the DataBase.
// *****************************************************************************

    handleRemoveMap(event)
    {
        this.setState({ display_div: false });
    }

    handleMarker(carte, event)
    {
        this.state.latitude = event.latLng.lat();
        this.state.longitude = event.latLng.lng();
        var location = new google.maps.LatLng(this.state.latitude, this.state.longitude);

        if (this.state.marker)
        {
            this.state.marker.setPosition(location);
        }
    }

    validateCity(event)
    {
        let value = 'longitude=' + this.state.longitude + '&latitude=' + this.state.latitude;
        axios(
        {
            method: 'POST',
            url: 'http://correction.matcha.bbichero.com/post.php',
            data: value
        }).then((response) =>
        {
            this.state.item_edit_list[this.state.current_index].value = response.data.value;
            this.state.item_edit_list[this.state.current_index].edit = false;
            this.setState({ item_edit_list: this.state.item_edit_list });
            this.setState({ display_div: false });
        });
    }

    handleCreateDiv(index, event)
    {
        this.state.display_div = true;
        this.setState({ display_div: this.state.display_div });
        this.setState({ current_index: index });
    }



    handleDisplayDiv()
    {
        if (this.state.display_div)
            return (
                React.createElement("div", null, 
                    React.createElement("div", {className: "map_closure_save", onClick: this.validateCity}), 
                    React.createElement("div", {className: "map_closure", onClick: this.handleRemoveMap}), 
                    React.createElement("div", {className: "map_google", id: "carte"})
                )
            );
        else
        {
            this.state.display_map = false;
            return (React.createElement("div", null));
        }
    }

    handleCreateMap()
    {
        var latlng  = new google.maps.LatLng(this.state.latitude, this.state.longitude);
        var options = {
            center:           latlng,
            zoom:             19,
            mapTypeId:        google.maps.MapTypeId.ROADMAP,
            zoomControl:      true,
            scaleControl:     true,
            disableDefaultUI: true
        };
        var carte   = new google.maps.Map(document.getElementById("carte"), options);
        var marker  = new google.maps.Marker({
            position:  latlng,
            map:       carte,
            title:     'Click to zoom',
            draggable: true,
            animation: google.maps.Animation.DROP
        });
        this.state.marker = marker;
        google.maps.event.addListener(carte, "click", this.handleMarker.bind(this, carte));
    }


// ********************
// END: Map feature.
// ********************

    handleEditChange(index, event)
    {
        this.state.item_edit_list[index].new_value = event.target.value;
        this.setState({ item_edit_list: this.state.item_edit_list });
    }

    handleEditProfile(data_type, index, event)
    {
        let value = 'data_type=' + data_type +
                    '&value=' + this.state.item_edit_list[index].new_value;
        if (this.state.item_edit_list[index].new_value !== this.state.item_edit_list[index].value)
        {
            axios(
            {
                method: 'POST',
                url: 'http://correction.matcha.bbichero.com/post.php',
                data: value
            }).then((response) =>
            {
                if (response.data.value === this.state.item_edit_list[index].new_value
                    && response.data.data_type === data_type)
                {
                    this.state.item_edit_list[index].value = response.data.value;
                    this.state.item_edit_list[index].edit = false;
                    this.setState({ item_edit_list: this.state.item_edit_list });
                }
            });
        }
        else
            this.state.item_edit_list[index].edit = false;
        this.setState({ item_edit_list: this.state.item_edit_list });
    }

    displayInfo(class_name, data_type, field_type, radio_fields)
    {
        let index = this.state.item_edit_list.findIndex(x => x.name == data_type);

        if (this.state.item_edit_list[index].edit === false)
            return (
                React.createElement("div", null, 
                    React.createElement("div", {className: class_name}, this.state.item_edit_list[index].value, 
                        React.createElement("img", {src: this.state.resources_path + "/edit.png", 
                             onClick: (data_type === "city") ? this.handleCreateDiv.bind(this, index) : this.handleMorsay.bind(this, data_type, index), 
                             width: "20px", 
                             className: "over_map"}
                        )
                    )

                )
            );
        else
            return (
                React.createElement("div", null, 
                    this.handleFieldType(index, data_type, field_type, radio_fields), 
                    React.createElement("img", {src: this.state.resources_path + "/check.png", 
                         onClick: this.handleEditProfile.bind(this, data_type, index), 
                         width: "20px"}
                    )
                )

            );
    }

    handleFieldType(index, data_type, field_type, select_fields = null)
    {
        if (field_type === "text")
            return (
                React.createElement("input", {className: "content", type: "text", 
                        value: this.state.item_edit_list[index].new_value, 
                        onChange: this.handleEditChange.bind(this, index)}
                ));
        else if (field_type === "radio")
        {
            const radios = select_fields.map((field, key) =>
            {
                if (field === this.state.item_edit_list[index].value)
                    return (React.createElement("input", {type: "radio", name: data_type, 
                                    value: field, checked: true, 
                                    onChange: this.handleEditChange.bind(this, index)}));
                else
                    return (React.createElement("input", {type: "radio", name: data_type, 
                                    value: field, 
                                    onChange: this.handleEditChange.bind(this, index)}));
            });
            return radios;
        }
        else if (field_type === "email")
            return (
                React.createElement("input", {className: "content", type: "email", 
                        value: this.state.item_edit_list[index].new_value, 
                        onChange: this.handleEditChange.bind(this, index)}
                ));
        else if (field_type === "select")
        {
            const select = select_fields.map((field, key) =>
            {
                //if (field === this.state.item_edit_list[index].value)
                //    return (<option selected="selected" key={key}>{field}</option>);
                //else
                    return (React.createElement("option", {key: key}, field));
            });
            return (
                React.createElement("select", {name: data_type, width: "40px", 
                        // className="pure-input-1-2"
                        value: this.state.item_edit_list[index].new_value, 
                        onChange: this.handleEditChange.bind(this, index)}, 
                        React.createElement("optgroup", {label: "Choose a value"}, "Option Group "), 
                        select
                )

            );
        }
    }

    handleMorsay(data_type, index, event)
    {
        if (this.state.item_edit_list[index].edit === false)
            this.state.item_edit_list[index].edit = true;
        else
            this.state.item_edit_list[index].edit = false;

        this.setState({ state_array: this.state.item_edit_list });
    }

// ****************************************
// Upload feature for user profile picture.
// ****************************************


    handleUpload(event)
    {
        this.setState({pp_clicked: true})
        document.getElementById('upload').click();
    }
    handleUpload_l(event)
    {
        this.setState({pp_clicked_l: true})
        document.getElementById('upload_l').click();
    }
    handleUpload_r(event)
    {
        this.setState({pp_clicked_r: true})
        document.getElementById('upload_r').click();
    }


    handleDelete(i)
    {
      var tags = this.state.tags.slice(0);
      let value = "type=del_tag&username=" + this.props.username + "&tag=" + tags[i].name;
      axios(
      {
          method: 'POST',
          url: 'http://correction.matcha.bbichero.com/post.php',
          data: value
      }).then((response) =>
      {
          if (response.data === value)
          {
              tags.splice(i, 1);
              this.setState({ tags });
          }
      });

    }

    handleAddition(tag)
    {
        const tags = this.state.tags.concat(tag);
        let value = "type=add_tag&username=" + this.props.username + "&tag=" + tag.name;
        axios(
        {
            method: 'POST',
            url: 'http://correction.matcha.bbichero.com/post.php',
            data: value
        }).then((response) =>
        {
            if (response.data === value)
                this.setState({ tags: tags });
        });
    }

    handleChangeFile()
    {
        this.refs.form.submit();
    }

    handleDisplayHistoryBar(notif_list)
    {
        const notifs = notif_list.map((notif, index) => {
            var public_profile = "../../public_profile.php?profile_username=" + notif.username;
            // this.state.url_path + public_profile
            return (
                React.createElement("div", {key: index}, 
                    React.createElement("div", null, 
                        React.createElement("a", {style: {color: 'black'}, target: "_black", href: this.state.url_path + public_profile}, 
                        React.createElement("div", {className: "profiles_div", 
                              style:  (notif.prez) ? {width: 'null', height: 'null', background: 'url(' + notif.prez + ')', backgroundSize: 'cover'} : {}}, 
                              notif.content
                        )
                        )
                    )
                )
            );
        });
        return notifs;
    }

// *****************************************************
// Render of the page: Render of the User Settings page.
// *****************************************************

    render()
    {
        const { tags, suggestions } = this.state;
        return (
            React.createElement("div", {className: "pure-form"}, 
                React.createElement("div", {className: "setting_top_div"}, 
                    React.createElement("div", {className: "left_panel panel"}, 
                        React.createElement("a", {href: this.state.url_path + "/index.php"}, 
                            React.createElement("img", {className: "home_s", src: this.state.resources_path + "/home.png"})
                        ), React.createElement("br", null), React.createElement("br", null), React.createElement("br", null), React.createElement("br", null), 
                        React.createElement("a", {href: this.state.url_path + "/messages.php"}, 
                            React.createElement("img", {className: "notif_s", src: this.state.resources_path + "/message.png"})
                        )
                    ), 
                    React.createElement("div", {className: "right_panel panel"}, 
                        React.createElement("a", {style: {border: "1px solid black"}, className: "pure-button", href: "/user_settings.php", name: "settings"}, "Settings"), 
                            React.createElement("br", null), React.createElement("br", null), React.createElement("br", null), React.createElement("br", null), 
                        React.createElement("a", {style: {border: "1px solid black"}, className: "pure-button", href: "/log_out.php", name: "logout"}, "Logout  ")
                    ), 
                    React.createElement("form", {name: "form_upload", action: "user_settings.php", method: "post", encType: "multipart/form-data", ref: "form"}, 
                        React.createElement("div", {className: "center"}, 
                            React.createElement("input", {onChange: this.handleChangeFile.bind(this), type: "file", id: "upload_r", name: "upload_r", style: {display: "none"}}), 
                            React.createElement("img", {className: "alt_picture_r", 
                                   src: (this.props.alt2) ? this.props.alt2 : "../../resources/empty_profile.png", 
                                   onClick: this.handleUpload_r})
                        ), 
                        React.createElement("div", {className: "center"}, 
                            React.createElement("input", {onChange: this.handleChangeFile.bind(this), type: "file", id: "upload_l", name: "upload_l", style: {display: "none"}}), 
                            React.createElement("img", {className: "alt_picture_l", 
                                    src: (this.props.alt1) ? this.props.alt1 : "../../resources/empty_profile.png", 
                                    onClick: this.handleUpload_l})
                        ), 
                        React.createElement("div", {className: "center"}, 
                            React.createElement("input", {onChange: this.handleChangeFile.bind(this), type: "file", id: "upload", name: "upload", style: {display: "none"}}), 
                            React.createElement("img", {className: "user_picture", 
                                   src: (this.props.prez) ? this.props.prez : "../../resources/empty_profile.png", 
                                   onClick: this.handleUpload})
                        )
                    ), 
                    React.createElement("div", {className: "right_panel panel"}
                    ), 
                    React.createElement("div", {className: "container"}, 
                        React.createElement("div", {className: "filled animated slideInLeft", style: {width: this.props.percent_xp + "%"}}), 
                        React.createElement("div", {className: "pex"}, "Level: ", this.props.level, " - ", this.props.percent_xp, "%")
                    ), 
                    React.createElement("br", null), React.createElement("br", null)
                ), 
                React.createElement("div", {className: "setting_left_div blocks"}, 
                    React.createElement("div", {className: "title_settings_blocks"}, "Public Informations"), 
                    React.createElement("div", {className: "bio"}, 
                        React.createElement("div", {className: "subtitle_settings_blocks"}, "Bio: "), 
                        this.displayInfo("content", "bio", "text")
                    ), 
                    React.createElement("div", {className: "gender_chan"}, 
                        React.createElement("div", {className: "subtitle_settings_blocks"}, "Gender: "), 
                        this.displayInfo("content", "gender", "select", Array("M", "F"))
                    ), 
                    React.createElement("div", {className: "orientation_chan"}, 
                        React.createElement("div", {className: "subtitle_settings_blocks"}, "Orientation: "), 
                        this.displayInfo("content", "orientation", "select", Array("H", "B", "T"))
                    ), 
                    React.createElement("div", {className: "tags"}, 
                        React.createElement("div", {className: "subtitle_settings_blocks"}, "Tags:"), 
                        React.createElement("div", {className: "content"}, React.createElement(Tags, {
                                tags: this.state.tags, 
                                suggestions: this.state.suggestions, 
                                handleDelete: this.handleDelete, 
                                handleAddition: this.handleAddition, 
                                placeholder: "Add     ", 
                                allowNew: true, allowBackspace: false}))
                    )
                ), 
                React.createElement("div", {className: "setting_right_div blocks"}, 
                    React.createElement("div", {className: "title_settings_blocks"}, "Private Informations"), 
                    React.createElement("div", {className: "email"}, 
                        React.createElement("div", {className: "subtitle_settings_blocks"}, "First Name:"), 
                        this.displayInfo("content", "first_name", "text")
                    ), 
                    React.createElement("div", {className: "email"}, 
                        React.createElement("div", {className: "subtitle_settings_blocks"}, "Last Name:"), 
                        this.displayInfo("content", "last_name", "text")
                    ), 
                    React.createElement("div", {className: "email"}, 
                        React.createElement("div", {className: "subtitle_settings_blocks"}, "Age:"), 
                        this.displayInfo("content", "age", "text")
                    ), 
                    React.createElement("div", {className: "email"}, 
                        React.createElement("div", {className: "subtitle_settings_blocks"}, "Email:"), 
                        this.displayInfo("content", "email", "email")
                    ), 
                    React.createElement("div", {className: "phone"}, 
                        React.createElement("div", {className: "subtitle_settings_blocks"}, "Phone number:"), 
                        this.displayInfo("content", "phone", "text")
                    ), 
                    React.createElement("div", {className: "city"}, 
                        React.createElement("div", {className: "subtitle_settings_blocks"}, "City: "), 
                        this.displayInfo("content", "city")
                    )
                ), 
                React.createElement("div", {className: "setting_bottom_div"}, 
                    React.createElement("center", null, this.handleDisplayHistoryBar(this.props.notif_list))
                ), 
                this.handleDisplayDiv(), 
                React.createElement("div", {id: "footer"}, this.state.notification)
            )
        );
    }
}


// *************************
// End: Class User_info_top.
// *************************

if (typeof module != 'undefined')
{
    module.exports = User_info_top;
}
else
{
    window.User_info_top = User_info_top;
}


// **********
// END: File.
// **********
