var React = window.React ? window.React : require('react');

class Auth_alert extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            error_type: this.props.error_type,
            auth_type: this.props.auth_type,
            display: false,
        };

        if (this.state.error_type !== null)
            this.state.display = true;

        this.handleDisplay = this.handleDisplay.bind(this);
    }

    handleDisplay()
    {
        if (this.state.display === true)
            this.setState({display: false});
        else
            this.setState({display: true});
    }

    render()
    {
        let error_message;
        if (this.state.error_type === "unknow_user")
            error_message = "No user match in our database, please create a account";
        if (this.state.error_type === "intern")
            error_message = "Internal error please try later (All admin will be notified)";
        if (this.state.error_type === "bad_paswd")
            error_message = "Incorrect password please try again";
        if (this.state.error_type === "password_miss_match")
            error_message = "2 password enter miss-match, please try again";
        if (this.state.error_type === "know_email")
            error_message = "Email already exist in our database";
        if (this.state.error_type === "know_user")
            error_message = "Username already exist in our database";

        if (this.state.display === true)
        {
            return (
                <div className="alert warning">
                    <span className="closebtn" onClick={this.handleDisplay}>&times;</span>
                    <strong>Warning! </strong> {error_message}
                    <br />
                </div>
            );
        }
        else
            return (<div></div>);
    }
}

if (typeof module != 'undefined') {
  module.exports = Auth_alert;
} else {
  window.Auth_alert = Auth_alert;
}
