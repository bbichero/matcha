var React = window.React ? window.React : require('react');
var axios = require('axios');
var Tags  = require('react-tag-autocomplete');
var socket = io.connect('http://matcha.bbichero.com:8080');

class User_info_top extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            url_path:       this.props.url_path,
            resources_path: this.props.resources_path,
            item_edit_list: this.props.item_edit_list,
            latitude:       this.props.lat,
            longitude:      this.props.lon,
            notification:   true,
            piture:         false,
            import_img:     false,
            display_div:    false,
            pp_clicked:     false,
            pp_clicked_l:   false,
            pp_clicked_r:   false,
            marker:         null,
            current_index:  null,
            tags:           (this.props.tags) ? (this.props.tags) : [{ id: 1, name:  "Sample"}],
            suggestions:    (this.props.suggestions) ? Object.keys(this.props.suggestions).map((key) => this.props.suggestions[key]) : [{ id: 1, name: "Kaplas" }, { id: 2, name: "Mangos" }, { id: 5, name: "Lemons" }, { id: 6, name: "Apricots" }],
        };

        this.state.item_edit_list.forEach((element, index) =>
        {
            this.state.item_edit_list[index].edit = false;
            this.state.item_edit_list[index].new_value = this.state.item_edit_list[index].value;
        });

        this.handleCreateDiv = this.handleCreateDiv.bind(this);
        this.handleRemoveMap = this.handleRemoveMap.bind(this);
        this.handleUpload    = this.handleUpload.bind(this);
        this.handleUpload_l  = this.handleUpload_l.bind(this);
        this.handleUpload_r  = this.handleUpload_r.bind(this);
        this.validateCity    = this.validateCity.bind(this);
        this.handleDelete    = this.handleDelete.bind(this);
        this.handleAddition  = this.handleAddition.bind(this);
        this.handleReceiveNotification = this.handleReceiveNotification.bind(this);

        socket.emit('connection_user', this.props.src_username);
    }

    componentDidMount()
    {
        socket.on('notification', this.handleReceiveNotification);
    }

    componentDidUpdate()
    {
        if (this.state.display_div)
            this.handleCreateMap();
    }

    handleReceiveNotification(type, message, event)
    {
        this.setState({notification : null});
        // Get a notification when an user sends you a message.
        if (type === "message")
        {
            this.state.notification =
            <div id="notification" className="notification_container animated fadeIn">
                <div className="notification base">
                    <div className="base"><img className="notification_pp base" src={message.prez ? message.prez : "resources/empty_profile.png"} /></div>
                    <h3 className="notification-title base">{message.src_username} sent you a message !</h3>
                    <p className="notification-message base">{message.body}</p>
                </div>
            </div>;
            this.setState({notification : this.state.notification});

            var temp = document.getElementById("notification");
            setTimeout(function(){temp.className += " fadeOut";}, 4000);
        }
        // Get a notification when an user likes your profile.
        if (type === "like")
        {
            this.state.notification =
            <div id="notification" className="notification_container animated fadeIn">
                <div className="notification base">
                    <div className="base"><img className="notification_pp base" src={message.prez ? message.prez : "resources/empty_profile.png"} /></div>
                    <h3 className="notification-title base"> Congratulations !</h3>
                    <p className="notification-message base">{message.src_username} liked you !</p>
                </div>
            </div>;
            this.setState({notification : this.state.notification});

            var temp = document.getElementById("notification");
            setTimeout(function(){temp.className += " fadeOut";}, 4000);
        }
        // Get a notification when an user match with you by liking your profile.
        if (type === "match")
        {
            this.state.notification =
            <div id="notification" className="notification_container animated fadeIn">
                <div className="notification base">
                    <div className="base"><img className="notification_pp base" src={message.prez ? message.prez : "resources/empty_profile.png"} /></div>
                    <h3 className="notification-title base"> Congratulations !</h3>
                    <p className="notification-message base">You've matched with you {message.src_username} !</p>
                </div>
            </div>;
            this.setState({notification : this.state.notification});

            var temp = document.getElementById("notification");
            setTimeout(function(){temp.className += " fadeOut";}, 4000);
        }
        // Get a notification when a matched user unlikes you.
        if (type === "unmatch")
        {
            this.state.notification =
            <div id="notification" className="notification_container animated fadeIn">
                <div className="notification base">
                    <div className="base"><img className="notification_pp base" src={message.prez ? message.prez : "resources/empty_profile.png"} /></div>
                    <h3 className="notification-title base"> Damn Daniel, match lost !</h3>
                    <p className="notification-message base">{message.src_username} unlikes you !</p>
                </div>
            </div>;
            this.setState({notification : this.state.notification});

            var temp = document.getElementById("notification");
            setTimeout(function(){temp.className += " fadeOut";}, 4000);
        }
        // Get a notification when an user is on your profile page.
        if (type === "connected_on")
        {
            this.state.notification =
            <div id="notification" className="notification_container animated fadeIn">
                <div className="notification base">
                    <div className="base"><img className="notification_pp base" src={message.prez ? message.prez : "resources/empty_profile.png"} /></div>
                    <h3 className="notification-title base"> Hey !</h3>
                    <p className="notification-message base">{message.src_username} is watching your profile !</p>
                </div>
            </div>;

            this.setState({notification : this.state.notification});

            var temp = document.getElementById("notification");
            setTimeout(function(){temp.className += " fadeOut";}, 4000);
        }
    }


// *****************************************************************************
// Map feature: Create a map with the Google API who permit to change the user
// location with a marker system validated with a check button, cancel in an
// other hand. The feature update coordinates and the city in the DataBase.
// *****************************************************************************

    handleRemoveMap(event)
    {
        this.setState({ display_div: false });
    }

    handleMarker(carte, event)
    {
        this.state.latitude = event.latLng.lat();
        this.state.longitude = event.latLng.lng();
        var location = new google.maps.LatLng(this.state.latitude, this.state.longitude);

        if (this.state.marker)
        {
            this.state.marker.setPosition(location);
        }
    }

    validateCity(event)
    {
        let value = 'longitude=' + this.state.longitude + '&latitude=' + this.state.latitude;
        axios(
        {
            method: 'POST',
            url: 'http://matcha.bbichero.com/post.php',
            data: value
        }).then((response) =>
        {
            this.state.item_edit_list[this.state.current_index].value = response.data.value;
            this.state.item_edit_list[this.state.current_index].edit = false;
            this.setState({ item_edit_list: this.state.item_edit_list });
            this.setState({ display_div: false });
        });
    }

    handleCreateDiv(index, event)
    {
        this.state.display_div = true;
        this.setState({ display_div: this.state.display_div });
        this.setState({ current_index: index });
    }



    handleDisplayDiv()
    {
        if (this.state.display_div)
            return (
                <div>
                    <div className="map_closure_save" onClick={this.validateCity}></div>
                    <div className="map_closure"      onClick={this.handleRemoveMap}></div>
                    <div className="map_google"       id="carte"></div>
                </div>
            );
        else
        {
            this.state.display_map = false;
            return (<div></div>);
        }
    }

    handleCreateMap()
    {
        var latlng  = new google.maps.LatLng(this.state.latitude, this.state.longitude);
        var options = {
            center:           latlng,
            zoom:             19,
            mapTypeId:        google.maps.MapTypeId.ROADMAP,
            zoomControl:      true,
            scaleControl:     true,
            disableDefaultUI: true
        };
        var carte   = new google.maps.Map(document.getElementById("carte"), options);
        var marker  = new google.maps.Marker({
            position:  latlng,
            map:       carte,
            title:     'Click to zoom',
            draggable: true,
            animation: google.maps.Animation.DROP
        });
        this.state.marker = marker;
        google.maps.event.addListener(carte, "click", this.handleMarker.bind(this, carte));
    }


// ********************
// END: Map feature.
// ********************

    handleEditChange(index, event)
    {
        this.state.item_edit_list[index].new_value = event.target.value;
        this.setState({ item_edit_list: this.state.item_edit_list });
    }

    handleEditProfile(data_type, index, event)
    {
        let value = 'data_type=' + data_type +
                    '&value=' + this.state.item_edit_list[index].new_value;
        if (this.state.item_edit_list[index].new_value !== this.state.item_edit_list[index].value)
        {
            axios(
            {
                method: 'POST',
                url: 'http://matcha.bbichero.com/post.php',
                data: value
            }).then((response) =>
            {
                if (response.data.value === this.state.item_edit_list[index].new_value
                    && response.data.data_type === data_type)
                {
                    this.state.item_edit_list[index].value = response.data.value;
                    this.state.item_edit_list[index].edit = false;
                    this.setState({ item_edit_list: this.state.item_edit_list });
                }
            });
        }
        else
            this.state.item_edit_list[index].edit = false;
        this.setState({ item_edit_list: this.state.item_edit_list });
    }

    displayInfo(class_name, data_type, field_type, radio_fields)
    {
        let index = this.state.item_edit_list.findIndex(x => x.name == data_type);

        if (this.state.item_edit_list[index].edit === false)
            return (
                <div>
                    <div className={class_name}>{this.state.item_edit_list[index].value}
                        <img src={this.state.resources_path + "/edit.png"}
                             onClick={(data_type === "city") ? this.handleCreateDiv.bind(this, index) : this.handleMorsay.bind(this, data_type, index)}
                             width="20px"
                             className="over_map"
                        />
                    </div>

                </div>
            );
        else
            return (
                <div>
                    {this.handleFieldType(index, data_type, field_type, radio_fields)}
                    <img src={this.state.resources_path + "/check.png"}
                         onClick={this.handleEditProfile.bind(this, data_type, index)}
                         width="20px"
                    />
                </div>

            );
    }

    handleFieldType(index, data_type, field_type, select_fields = null)
    {
        if (field_type === "text")
            return (
                <input  className="content" type="text"
                        value={this.state.item_edit_list[index].new_value}
                        onChange={this.handleEditChange.bind(this, index)}
                />);
        else if (field_type === "radio")
        {
            const radios = select_fields.map((field, key) =>
            {
                if (field === this.state.item_edit_list[index].value)
                    return (<input  type="radio" name={data_type}
                                    value={field} checked
                                    onChange={this.handleEditChange.bind(this, index)}/>);
                else
                    return (<input  type="radio" name={data_type}
                                    value={field}
                                    onChange={this.handleEditChange.bind(this, index)}/>);
            });
            return radios;
        }
        else if (field_type === "email")
            return (
                <input  className="content" type="email"
                        value={this.state.item_edit_list[index].new_value}
                        onChange={this.handleEditChange.bind(this, index)}
                />);
        else if (field_type === "select")
        {
            const select = select_fields.map((field, key) =>
            {
                //if (field === this.state.item_edit_list[index].value)
                //    return (<option selected="selected" key={key}>{field}</option>);
                //else
                    return (<option key={key}>{field}</option>);
            });
            return (
                <select name={data_type} width="40px"
                        // className="pure-input-1-2"
                        value={this.state.item_edit_list[index].new_value}
                        onChange={this.handleEditChange.bind(this, index)}>
                        <optgroup label="Choose a value">Option Group </optgroup>
                        {select}
                </select>

            );
        }
    }

    handleMorsay(data_type, index, event)
    {
        if (this.state.item_edit_list[index].edit === false)
            this.state.item_edit_list[index].edit = true;
        else
            this.state.item_edit_list[index].edit = false;

        this.setState({ state_array: this.state.item_edit_list });
    }

// ****************************************
// Upload feature for user profile picture.
// ****************************************


    handleUpload(event)
    {
        this.setState({pp_clicked: true})
        document.getElementById('upload').click();
    }
    handleUpload_l(event)
    {
        this.setState({pp_clicked_l: true})
        document.getElementById('upload_l').click();
    }
    handleUpload_r(event)
    {
        this.setState({pp_clicked_r: true})
        document.getElementById('upload_r').click();
    }


    handleDelete(i)
    {
      var tags = this.state.tags.slice(0);
      let value = "type=del_tag&username=" + this.props.username + "&tag=" + tags[i].name;
      axios(
      {
          method: 'POST',
          url: 'http://matcha.bbichero.com/post.php',
          data: value
      }).then((response) =>
      {
          if (response.data === value)
          {
              tags.splice(i, 1);
              this.setState({ tags });
          }
      });

    }

    handleAddition(tag)
    {
        const tags = this.state.tags.concat(tag);
        let value = "type=add_tag&username=" + this.props.username + "&tag=" + tag.name;
        axios(
        {
            method: 'POST',
            url: 'http://matcha.bbichero.com/post.php',
            data: value
        }).then((response) =>
        {
            if (response.data === value)
                this.setState({ tags: tags });
        });
    }

    handleChangeFile()
    {
        this.refs.form.submit();
    }

    handleDisplayHistoryBar(notif_list)
    {
        const notifs = notif_list.map((notif, index) => {
            var public_profile = "../../public_profile.php?profile_username=" + notif.username;
            // this.state.url_path + public_profile
            return (
                <div key={index}>
                    <div>
                        <a style={{color: 'black'}} target={"_black"} href={this.state.url_path + public_profile}>
                        <div  className={"profiles_div"}
                              style={ (notif.prez) ? {width: 'null', height: 'null', background: 'url(' + notif.prez + ')', backgroundSize: 'cover'} : {}}>
                              {notif.content}
                        </div>
                        </a>
                    </div>
                </div>
            );
        });
        return notifs;
    }

// *****************************************************
// Render of the page: Render of the User Settings page.
// *****************************************************

    render()
    {
        const { tags, suggestions } = this.state;
        return (
            <div className="pure-form">
                <div className="setting_top_div">
                    <div className="left_panel panel">
                        <a href={this.state.url_path + "/index.php"}>
                            <img className="home_s" src={this.state.resources_path + "/home.png"}></img>
                        </a><br></br><br></br><br></br><br></br>
                        <a href={this.state.url_path + "/messages.php"}>
                            <img className="notif_s" src={this.state.resources_path + "/message.png"}></img>
                        </a>
                    </div>
                    <div className="right_panel panel">
                        <a style={{border: "1px solid black"}} className="pure-button" href={"/user_settings.php"}  name="settings">Settings</a>
                            <br></br><br></br><br></br><br></br>
                        <a style={{border: "1px solid black"}} className="pure-button" href={"/log_out.php"}  name="logout">Logout  </a>
                    </div>
                    <form name="form_upload" action="user_settings.php" method="post" encType="multipart/form-data" ref="form">
                        <div className="center">
                            <input onChange={this.handleChangeFile.bind(this)} type="file" id="upload_r" name="upload_r" style={{display: "none"}}/>
                            <img   className="alt_picture_r"
                                   src={(this.props.alt2) ? this.props.alt2 : "../../resources/empty_profile.png"}
                                   onClick={this.handleUpload_r}/>
                        </div>
                        <div className="center">
                            <input  onChange={this.handleChangeFile.bind(this)} type="file" id="upload_l" name="upload_l" style={{display: "none"}}/>
                            <img    className="alt_picture_l"
                                    src={(this.props.alt1) ? this.props.alt1 : "../../resources/empty_profile.png"}
                                    onClick={this.handleUpload_l}/>
                        </div>
                        <div className="center">
                            <input onChange={this.handleChangeFile.bind(this)} type="file" id="upload" name="upload" style={{display: "none"}}/>
                            <img   className="user_picture"
                                   src={(this.props.prez) ? this.props.prez : "../../resources/empty_profile.png"}
                                   onClick={this.handleUpload}/>
                        </div>
                    </form>
                    <div className="right_panel panel">
                    </div>
                    <div className="container">
                        <div className="filled animated slideInLeft" style={{width: this.props.percent_xp + "%" }}></div>
                        <div className="pex">Level: {this.props.level} - {this.props.percent_xp}%</div>
                    </div>
                    <br></br><br></br>
                </div>
                <div className="setting_left_div blocks" >
                    <div className="title_settings_blocks">Public Informations</div>
                    <div className="bio">
                        <div className="subtitle_settings_blocks">Bio: </div>
                        {this.displayInfo("content", "bio", "text")}
                    </div>
                    <div className="gender_chan">
                        <div className="subtitle_settings_blocks">Gender: </div>
                        {this.displayInfo("content", "gender", "select", Array("M", "F"))}
                    </div>
                    <div className="orientation_chan">
                        <div className="subtitle_settings_blocks">Orientation: </div>
                        {this.displayInfo("content", "orientation", "select", Array("H", "B", "T"))}
                    </div>
                    <div className="tags">
                        <div className="subtitle_settings_blocks">Tags:</div>
                        <div className="content"><Tags
                                tags={this.state.tags}
                                suggestions={this.state.suggestions}
                                handleDelete={this.handleDelete}
                                handleAddition={this.handleAddition}
                                placeholder={"Add     "}
                                allowNew={true} allowBackspace={false} /></div>
                    </div>
                </div>
                <div className="setting_right_div blocks">
                    <div className="title_settings_blocks">Private Informations</div>
                    <div className="email">
                        <div className="subtitle_settings_blocks">First Name:</div>
                        {this.displayInfo("content", "first_name", "text")}
                    </div>
                    <div className="email">
                        <div className="subtitle_settings_blocks">Last Name:</div>
                        {this.displayInfo("content", "last_name", "text")}
                    </div>
                    <div className="email">
                        <div className="subtitle_settings_blocks">Age:</div>
                        {this.displayInfo("content", "age", "text")}
                    </div>
                    <div className="email">
                        <div className="subtitle_settings_blocks">Email:</div>
                        {this.displayInfo("content", "email", "email")}
                    </div>
                    <div className="phone">
                        <div className="subtitle_settings_blocks">Phone number:</div>
                        {this.displayInfo("content", "phone", "text")}
                    </div>
                    <div className="city">
                        <div className="subtitle_settings_blocks">City: </div>
                        {this.displayInfo("content", "city")}
                    </div>
                </div>
                <div className="setting_bottom_div">
                    <center>{this.handleDisplayHistoryBar(this.props.notif_list)}</center>
                </div>
                {this.handleDisplayDiv()}
                <div id="footer">{this.state.notification}</div>
            </div>
        );
    }
}


// *************************
// End: Class User_info_top.
// *************************

if (typeof module != 'undefined')
{
    module.exports = User_info_top;
}
else
{
    window.User_info_top = User_info_top;
}


// **********
// END: File.
// **********
