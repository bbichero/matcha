var React = window.React ? window.React : require('react');
var axios    = require('axios');
var socket   = io.connect('http://matcha.bbichero.com:8080');

class Conversation_notif extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            notification: true,
            notif: this.props.count,
        };

        this.handleReceiveNotification = this.handleReceiveNotification.bind(this);
        socket.emit('connection_conversation_notif', this.props.src_username);
    }

    componentDidMount()
    {
        socket.on('notification', this.handleReceiveNotification);
    }

    handleReceiveNotification(type, message, event)
    {
        this.setState({notification : null});
        // Get a notification when an user sends you a message.
        if (type === "message")
        {
            var message_final = message.body.substring(0, 120);
            if (message.body.length > 120)
                message_final = message_final + "...";
            this.state.notification =
            <div id="notification" className="notification_container animated fadeIn">
                <div className="notification base">
                    <div className="base"><img className="notification_pp base" src={message.prez ? message.prez : "resources/empty_profile.png"} /></div>
                    <h3 className="notification-title base">{message.src_username} sent you a message !</h3>
                    <p className="notification-message base">{message_final}</p>
                </div>
            </div>;
            this.setState({notification : this.state.notification});

            var temp = document.getElementById("notification");
            setTimeout(function(){temp.className += " fadeOut";}, 4000);
        }
        // Get a notification when an user likes your profile.
        if (type === "like")
        {
            this.state.notification =
            <div id="notification" className="notification_container animated fadeIn">
                <div className="notification base">
                    <div className="base"><img className="notification_pp base" src={message.prez ? message.prez : "resources/empty_profile.png"} /></div>
                    <h3 className="notification-title base"> Congratulations !</h3>
                    <p className="notification-message base">{message.src_username} liked you !</p>
                </div>
            </div>;
            this.setState({notification : this.state.notification});

            var temp = document.getElementById("notification");
            setTimeout(function(){temp.className += " fadeOut";}, 4000);
        }
        // Get a notification when an user match with you by liking your profile.
        if (type === "match")
        {
            this.state.notification =
            <div id="notification" className="notification_container animated fadeIn">
                <div className="notification base">
                    <div className="base"><img className="notification_pp base" src={message.prez ? message.prez : "resources/empty_profile.png"} /></div>
                    <h3 className="notification-title base"> Congratulations !</h3>
                    <p className="notification-message base">You've matched with you {message.src_username} !</p>
                </div>
            </div>;
            this.setState({notification : this.state.notification});

            var temp = document.getElementById("notification");
            setTimeout(function(){temp.className += " fadeOut";}, 4000);
        }
        // Get a notification when a matched user unlikes you.
        if (type === "unmatch")
        {
            this.state.notification =
            <div id="notification" className="notification_container animated fadeIn">
                <div className="notification base">
                    <div className="base"><img className="notification_pp base" src={message.prez ? message.prez : "resources/empty_profile.png"} /></div>
                    <h3 className="notification-title base"> Damn Daniel, match lost !</h3>
                    <p className="notification-message base">{message.src_username} unlikes you !</p>
                </div>
            </div>;
            this.setState({notification : this.state.notification});

            var temp = document.getElementById("notification");
            setTimeout(function(){temp.className += " fadeOut";}, 4000);
        }
        // Get a notification when an user is on your profile page.
        if (type === "connected_on")
        {
            this.state.notification =
            <div id="notification" className="notification_container animated fadeIn">
                <div className="notification base">
                    <div className="base"><img className="notification_pp base" src={message.prez ? message.prez : "resources/empty_profile.png"} /></div>
                    <h3 className="notification-title base"> Hey !</h3>
                    <p className="notification-message base">{message.src_username} is watching your profile !</p>
                </div>
            </div>;

            this.setState({notification : this.state.notification});

            var temp = document.getElementById("notification");
            setTimeout(function(){temp.className += " fadeOut";}, 4000);
        }
    }

// ***************************************************************************** //
// This is the filter part who brings an adaptive result by the user parameters. //
// ***************************************************************************** //

    render()
    {
        return (
            <div>
                <div id="footer">{this.state.notification}</div>
            </div>
        );
    }
}

if (typeof module != 'undefined')
{
    module.exports = Conversation_notif;
}
else
{
    window.Conversation_notif = Conversation_notif;
}

// xxxxxxxxxxxx //
// End of page. //
// xxxxxxxxxxxx //
