var React = window.React ? window.React : require('react');

class Auth_form extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            sign_up_username: this.props.sign_up_username,
            sign_up_email: this.props.sign_up_email,
            sign_up_first_name: this.props.sign_up_first_name,
            sign_up_last_name: this.props.sign_up_last_name,
            auth_type: this.props.auth_type,
            reset_form: this.props.reset_form,
        };

        if (this.state.auth_type === "sign_in")
            this.state.auth_type = "sign_in";
        else
            this.state.auth_type = "sign_up";

        this.handleChangeForm = this.handleChangeForm.bind(this);
        this.handleResetForm  = this.handleResetForm.bind(this);
        this.sign_in          = this.sign_in.bind(this);
        this.sign_up          = this.sign_up.bind(this);
        this.reset_pass       = this.reset_pass.bind(this);
        this.reset_key        = this.reset_key.bind(this);
    }


    handleResetForm(event)
    {
        if (this.state.auth_type === "sign_in")
            this.setState({auth_type: "reset_pass"});
        else if (this.state.auth_type === "reset_pass")
            this.setState({auth_type: "sign_in"});
    }
    handleChangeForm(event)
    {
        if (this.state.auth_type === "sign_in")
            this.setState({auth_type: "sign_up"});
        else if (this.state.auth_type === "sign_up")
            this.setState({auth_type: "sign_in"});
    }

    sign_in()
    {
        return (
            <div>
                <br /><br />
                <img src="../../resources/log.png" alt="login" height="50" className="center" />
                <br /><br />
                <div className="pure-control-group">
                    <input className="sign_in_field" name="sign_in_username" type="text" placeholder="Username" required />
                </div>

                <div className="pure-control-group">
                    <input className="sign_in_field" name="sign_in_password" type="password" placeholder="Password" required />
                </div>
                <div className="center animated bounceIn">
                    <center><button type="submit" name="sign_in" className="pure-button">Sign in</button></center>
                </div>
                <br /><br />
                <div className="center animated bounceIn">
                    <center><button className="pure-button" onClick={this.handleChangeForm}>Sign up page</button></center>
                </div>
                <br /><br />
                <div className="center animated bounceIn">
                    <center><button className="pure-button" onClick={this.handleResetForm}>Forgot password ?</button></center>
                </div>
            </div>
        );
    }

    sign_up()
    {
        return (
            <div>
                <img src="../../resources/log.png" alt="login" height="50" className="center" />
                <br /><br />
                <div className="pure-control-group">
                    <input  className="sign_in_field"
                            name="sign_up_username"
                            defaultValue={this.state.sign_up_username}
                            type="text" placeholder="Username" required />
                </div>

                <div className="pure-control-group">
                    <input  className="sign_in_field"
                            name="sign_up_first_name"
                            defaultValue={this.state.sign_up_first_name}
                            type="text" placeholder="First Name" required />
                </div>

                <div className="pure-control-group">
                    <input  className="sign_in_field"
                            name="sign_up_last_name"
                            defaultValue={this.state.sign_up_last_name}
                            type="text" placeholder="Last Name" required />
                </div>

                <div className="pure-control-group">
                    <input  className="sign_in_field"
                            name="sign_up_email"
                            defaultValue={this.state.sign_up_email}
                            type="email" placeholder="Email address" required />
                </div>

                <div className="pure-control-group">
                    <input  className="sign_in_field"
                            name="sign_up_password" type="password" placeholder="Password" required />
                </div>

                <div className="pure-control-group">
                    <input  className="sign_in_field"
                            name="sign_up_password_confirm"
                            type="password" placeholder="Confirm password" required />
                </div>

                <div className="pure-controls">
                    <input name="sign_up_term" type="checkbox"/> <a href="https://www.youtube.com/embed/Gc2u6AFImn8?rel=0&autoplay=1" target="_blank">  I ve read the terms and conditions</a>
                    <br /><br />
                    <div className="center animated bounceIn">
                        <center><button type="submit" name="sign_up"  className="pure-button">Sign up</button></center>
                    </div>
                </div>
                <br /><br />
                <div className="center animated bounceIn">
                    <center><button className="pure-button" onClick={this.handleChangeForm}>Sign in page</button></center>
                </div>
            </div>
        );
    }

    reset_pass()
    {
        return (
            <div>
                <img src="../../resources/log.png" alt="login" height="50" className="center" />
                <br /><br />
                <div className="pure-control-group">
                    <input  className="sign_in_field"
                            name="reset_email"
                            defaultValue=""
                            type="email" placeholder="Email address" required />
                </div>

                <div className="pure-controls">
                    <br />
                    <div className="center animated bounceIn">
                        <center><button type="submit" name="reset"  className="pure-button">Validate</button></center>
                    </div>
                </div>
                <br /><br />
                <div className="center animated bounceIn">
                    <center><button className="pure-button" onClick={this.handleResetForm}>Sign in page</button></center>
                </div>
            </div>
        );
    }
    reset_key()
    {
        return (
            <div><form action="/index.php" method="post">
                <img src="../../resources/log.png" alt="login" height="50" className="center" />
                <br /><br />
                <div className="pure-control-group">
                    <input  className="sign_in_field"
                            name="reset_key_email"
                            defaultValue=""
                            type="email" placeholder="Email address" required />
                </div>
                <div className="pure-control-group">
                    <input  className="sign_in_field"
                            name="reset_password" type="password" placeholder="Password" required />
                </div>

                <div className="pure-control-group">
                    <input  className="sign_in_field"
                            name="reset_password_confirm"
                            type="password" placeholder="Confirm password" required />
                </div>
                <div className="pure-controls">
                    <br />
                    <div className="center animated bounceIn">
                        <center><button type="submit" name="reset_key"  className="pure-button">Validate</button></center>
                    </div>
                </div>
                <br /><br />
                <div className="center animated bounceIn">
                    <center><button className="pure-button" onClick={this.handleResetForm}>Sign in page</button></center>
                </div>
                <input type="hidden" name="q" value={this.state.reset_form}/>
            </form></div>
        );
    }

    render()
    {
        if (this.state.reset_form)
            return (this.reset_key());
        else if (this.state.auth_type === "sign_in")
            return (this.sign_in());
        else if (this.state.auth_type === "sign_up")
            return (this.sign_up());
        else if (this.state.auth_type === "reset_pass")
            return (this.reset_pass());

    }
}

if (typeof module != 'undefined') {
  module.exports = Auth_form;
} else {
  window.Auth_form = Auth_form;
}
