// *****************
//
// 42born2code Paris
// Project: matcha
// File: server.js
//
// *****************

var debug = 1;

var app = require('express')(),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server)
    axios = require('axios'),
    ent = require('ent');

var mysql = require('mysql');

var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'dev-matcha',
  password : 'matcha-42-project',
  database : 'dev_matcha'
});

connection.connect();

function updateSocketId(username, socket_id) {
    connection.query('UPDATE users SET socket_id = ? WHERE username = ?', [socket_id, username],
    function (error, results, fields) {
        if (error)
            throw error;
    });
}

function updateSocketIdNotif(username, socket_id) {
    connection.query('UPDATE users SET socket_id_notif = ? WHERE username = ?', [socket_id, username],
    function (error, results, fields) {
        if (error)
            throw error;
    });
}

function updateSocketIdConversation(username, socket_id) {
    connection.query('UPDATE users SET socket_id_conv = ? WHERE username = ?', [socket_id, username],
    function (error, results, fields) {
        if (error)
            throw error;
    });
}

function updateMessageStatus(src_username, dst_username, status) {
    connection.query('UPDATE messages INNER JOIN users src ON src.id = messages.id_user_src INNER JOIN users dst ON dst.id = messages.id_user_dst SET status = ? WHERE ((src.username = ? AND dst.username = ?) OR (dst.username = ? AND src.username = ?))', [status, src_username, dst_username, src_username, dst_username],
    function (error, results, fields) {
        if (error)
            throw error;
    });
}


// *****************************************************************************
// fs = require('fs');
//
// var options = {
//     key: fs.readFileSync('/etc/ssl/private/www.matcha.com.key.pem'),
//     cert: fs.readFileSync('/etc/ssl/certs/www.matcha.bbichero.com.cert.pem'),
//     ca: fs.readFileSync('/etc/ssl/certs/ca-chain.cert.pem'),
//     passphrase: '42-Matcha-13520',
//     requestCert: true
// };
// var server = require('https').createServer(options, app);
// Chargement de la page index.html
// *****************************************************************************


// *************************************************************
// Targets all the js files to handle the socket server service.
app.get('/', function (req, res)
{
    res.sendFile(__dirname + '/js-build');
});

// ******************************************************************************************
// Launch a socket server for get multiple events like notification, live status user, etc...
// ******************************************************************************************

var state = null;
io.sockets.on('connection', function (socket, username)
{
    // ********************************************
    // When a user connect itself (or reload page).
    socket.on('connection_user', function(username)
    {
        if (debug === 1) {
            console.log("User : " + username + " is connected");console.log("=========> socket of " + username + ":  " + socket.id);
        }
        state = username;
        updateSocketId(username, socket.id);
    })

    // ********************************************
    // When a user connect itself for ntif counter (or reload page).
    socket.on('connection_notif', function(username)
    {
        if (debug === 1) {
            console.log("User : " + username + " is connected");console.log("=========> socket notif of " + username + ":  " + socket.id);
        }
        state = username;
        updateSocketIdNotif(username, socket.id);
    })

    // ********************************************
    // When a user connect itself for notif in message page (or reload page).
    socket.on('connection_conversation_notif', function(username)
    {
        if (debug === 1) {
            console.log("User : " + username + " is connected");console.log("=========> socket notif conv of " + username + ":  " + socket.id);
        }
        state = username;
        updateSocketIdConversation(username, socket.id);
    })

    // ********************************************
    // When a user disconnect itself (or reload page).
    socket.on('disconnect', function()
    {
        if (debug === 1)
            console.log("User : " + state + " is disconnected");
        // updateSocketId(username, socket.id);
    })

    // *******************************************************
    // When a user open a conversation with an unseen message.
    socket.on('seen', function(conversation)
    {
        updateMessageStatus(conversation.src_username, conversation.dst_username, 'S');
        var query = connection.query('SELECT socket_id FROM users WHERE username = ?', [conversation.src_username]);
        query.on('error', function(err) {
            throw err;
        });

        query.on('result', function(row) {
            socket.broadcast.to(row.socket_id).emit('seen', {dst_username: conversation.dst_username, src_username: conversation.src_username});
        });
    })

    // ***************************
    // When the message is opened.
    socket.on('message_receipt_of', function (infos)
    {
        updateMessageStatus(infos.message.src_username, infos.message.dst_username, 'R');
        var query = connection.query('SELECT socket_id FROM users WHERE username = ?', [infos.message.src_username]);
        query.on('error', function(err) {
            throw err;
        });

        query.on('result', function(row) {
            socket.broadcast.to(row.socket_id).emit('message_receipt_of', {message: infos.message, active: infos.active});
        });
    })

    // ****************************************************************
    // Get the username of the user who send a message and transmit it.
    socket.on('message', function (message, usernames)
    {
        // Pair the socket_id with username.
        var query = connection.query('SELECT socket_id, socket_id_notif, socket_id_conv FROM users WHERE username = ?', [message.dst_username]);
        query.on('error', function(err) {
            throw err;
        });

        // Save in database the notification.
        var content = message.src_username + " send you a message.";
        var r_state = connection.query('INSERT INTO notifications (id_user_src, id_user_dst, content) VALUES ?', [[[usernames.id_src_username, usernames.id_dst_username, content]]]);
        r_state.on('error', function(err) {
            throw err;
        });

        query.on('result', function(row) {
            socket.broadcast.to(row.socket_id).emit('message', message);
            if (debug === 1){
                console.log("Server receive message : " + message.body); console.log("From user : " + message.src_username); console.log("To user : " + message.dst_username);
            }
            // Get a notification when an user sends you a message.
            socket.broadcast.to(row.socket_id).emit('notification', 'message', message);
            socket.broadcast.to(row.socket_id_conv).emit('notification', 'message', message);
            socket.broadcast.to(row.socket_id_notif).emit('notif_number', 'connected_on', usernames);
        });
    })

    // ******************************************************************
    // Get the status of the owner of the page where the current user is.

    socket.on('connection_status', function (usernames)
    {
        // Pair the socket_id with username.
        var query = connection.query('SELECT socket_id, last_signin_date FROM users WHERE username = ?', [usernames.dst_username]);
        query.on('error', function(err) {
            throw err;
        });
        var list = Object.keys(io.sockets.sockets);
        query.on('result', function(row)
        {
            if (debug === 1) {
                console.log("Info : Socket id of sendered user: " + socket.id); console.log("Info : List of connected users: " + list); console.log("Info : Socket id of targeted user: " + row.socket_id);
            }
            if (list.includes(row.socket_id)) {
                io.to(socket.id).emit('status', 'online', "Online");
                if (debug === 1)
                    console.log("User : " + usernames.dst_username + " is online ");
            }
            else {
                io.to(socket.id).emit('status', 'date', row.last_signin_date);
                if (debug === 1)
                    console.log("User : " + usernames.dst_username + " is offline ");
            }
        });
    })

    // *****************************************************
    // Notifications generated by actions on a profile page.
    // *****************************************************

    // ********************************************************
    // Get a notification when an user is on your profile page.
    socket.on('connected_on', function (usernames)
    {
        if (!usernames.blocked_by.includes(usernames.id_dst_username))
        {

            // Pair the socket_id with username.
            var query = connection.query('SELECT socket_id, socket_id_notif, socket_id_conv FROM users WHERE username = ?', [usernames.dst_username]);
            query.on('error', function(err) {
                throw err;
            });

            // Save in database the notification.
            var content = usernames.src_username + " look a glance on your profile.";
            var r_state = connection.query('INSERT INTO notifications (id_user_src, id_user_dst, content) VALUES ?', [[[usernames.id_src_username, usernames.id_dst_username, content]]]);
            r_state.on('error', function(err) {
                throw err;
            });


            query.on('result', function(row) {
                socket.broadcast.to(row.socket_id).emit('notification', 'connected_on', usernames);
                socket.broadcast.to(row.socket_id_conv).emit('notification', 'connected_on', usernames);
                socket.broadcast.to(row.socket_id_notif).emit('notif_number', 'connected_on', usernames);
                if (debug === 1) {
                    console.log("User : " + usernames.src_username + " is connected on " + usernames.dst_username + " profile.");console.log("====> id target: " + row.socket_id);console.log("====> id target: " + row.socket_id_conv);
                }
            });
        }
    })

    // ***************************************************
    // Get a notification when an user likes your profile.
    socket.on('like', function (usernames)
    {
        if (!usernames.blocked_by.includes(usernames.id_dst_username))
        {
            // Pair the socket_id with username.
            var query = connection.query('SELECT socket_id, socket_id_notif, socket_id_conv FROM users WHERE username = ?', [usernames.dst_username]);
            query.on('error', function(err) {
                throw err;
            });

            // Save in database the notification.
            var content = usernames.src_username + " liked you.";
            var r_state = connection.query('INSERT INTO notifications (id_user_src, id_user_dst, content) VALUES ?', [[[usernames.id_src_username, usernames.id_dst_username, content]]]);
            r_state.on('error', function(err) {
                throw err;
            });

            // Broadcast the action to the current user.
            query.on('result', function(row) {
                socket.broadcast.to(row.socket_id).emit('notification', 'like', usernames);
                socket.broadcast.to(row.socket_id_conv).emit('notification', 'like', usernames);
                socket.broadcast.to(row.socket_id_notif).emit('notif_number', 'connected_on', usernames);
                if (debug === 1) {
                    console.log("User : " + usernames.src_username + " liked " + usernames.dst_username + ".");
                }
            });
        }
    })

    // **********************************************************************
    // Get a notification when an user match with you by liking your profile.
    socket.on('match', function (usernames)
    {
        if (!usernames.blocked_by.includes(usernames.id_dst_username))
        {
            // Pair the socket_id with username.
            var query = connection.query('SELECT socket_id, socket_id_notif, socket_id_conv FROM users WHERE username = ?', [usernames.dst_username]);
            query.on('error', function(err) {
                throw err;
            });

            // Save in database the notification.
            var content = "You match with " + usernames.src_username;
            var r_state = connection.query('INSERT INTO notifications (id_user_src, id_user_dst, content) VALUES ?', [[[usernames.id_src_username, usernames.id_dst_username, content]]]);
            r_state.on('error', function(err) {
                throw err;
            });

            // Broadcast the action to the current user.
            query.on('result', function(row) {
                socket.broadcast.to(row.socket_id).emit('notification', 'match', usernames);
                socket.broadcast.to(row.socket_id_conv).emit('notification', 'match', usernames);
                socket.broadcast.to(row.socket_id_notif).emit('notif_number', 'connected_on', usernames);
                if (debug === 1)
                    console.log("User : " + usernames.src_username + " matched " + usernames.dst_username + ".");
            });
        }
    })

    // ***************************************************
    // Get a notification when a matched user unlikes you.
    socket.on('unmatch', function (usernames)
    {
        if (!usernames.blocked_by.includes(usernames.id_dst_username))
        {
            // Pair the socket_id with username.
            var query = connection.query('SELECT socket_id, socket_id_notif, socket_id_conv FROM users WHERE username = ?', [usernames.dst_username]);
            query.on('error', function(err) {
                throw err;
            });

            // Save in database the notification.
            var content = "Unmatch with " + usernames.src_username;
            var r_state = connection.query('INSERT INTO notifications (id_user_src, id_user_dst, content) VALUES ?', [[[usernames.id_src_username, usernames.id_dst_username, content]]]);
            r_state.on('error', function(err) {
                throw err;
            });

            // Broadcast the action to the current user.
            query.on('result', function(row) {
                socket.broadcast.to(row.socket_id).emit('notification', 'unmatch', usernames);
                socket.broadcast.to(row.socket_id_conv).emit('notification', 'unmatch', usernames);
                socket.broadcast.to(row.socket_id_notif).emit('notif_number', 'connected_on', usernames);
                if (debug === 1)
                    console.log("User : " + usernames.src_username + " unmatched " + usernames.dst_username + ".");
            });
        }
    })
});

// connection.end();
server.listen(8080);
