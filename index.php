<?hh

// Define page name
$page_name = "index";

// Require Mandatory include
require("core/ini.php");

// async function simple_query($connection): Awaitable<array>
// {
//     $result = await $connection->queryf('SELECT * FROM items');
//     return $result->mapRows()->toArray();
// }

// Create Auth alert class for sign-up and sign-in
class :People-near extends :x:element
{
    use XHPHelpers;
    use XHPReact;

    attribute
    :xhp:html-element,
    array profiles_near @required,
    string src_username @required;

    protected function render(): XHPRoot
    {
        $this->constructReactInstance(
        'People_near',
            Map {
                'profiles_near' => $this->:profiles_near,
                'src_username' => $this->:src_username,
            }
        );
        return <div id={$this->getID()} />;
    }
}
// Get current user id.
$src_id = $PDO->query("SELECT id FROM users WHERE username = '" . $_SESSION['auth']['username'] . "'")->fetchColumn();

// Get back possition of current user
$position_cur_user = $PDO->query("SELECT lat, lon FROM users WHERE username = '" . $_SESSION['auth']['username'] . "'")->fetch(PDO::FETCH_NUM);

// If user request a search
if (isset($_POST['search']))
{
    $sql_search = "SELECT users.id, username, gender, orientation, prez, age, bio, level, lat, lon, id_tags
                    FROM users
                    INNER JOIN profile_stats ON users.id = id_user
                    WHERE 1 ";

    if(isset($_POST['gender']))
    {
        if ($_POST['gender'] == "male")
            $sql_search .= "AND gender = 'M'";
        else if ($_POST['gender'] == "female")
            $sql_search .= "AND gender = 'F'";
        else
            $sql_search .= "AND gender = 'O'";
    }

    if(isset($_POST['orientation']))
    {
        if ($_POST['orientation'] == "straight")
            $sql_search .= "AND orientation = 'H'";
        else if ($_POST['orientation'] == "gay")
            $sql_search .= "AND orientation = 'G'";
        else
            $sql_search .= "AND orientation = 'B'";
    }

    if (isset($_POST['age_min']) || isset($POST['age_max']))
    {
        if (isset($_POST['age_min']) && is_numeric($_POST['age_min']))
            $age_min = (int)$_POST['age_min'];
        else
            $age_min = 0;
        if (isset($_POST['age_max']) && is_numeric($_POST['age_max']))
            $age_max = (int)$_POST['age_max'];
        else
            $age_max = 99;

        $sql_search .= "AND age BETWEEN '$age_min' AND '$age_max'";
    }

    if (isset($_POST['lvl_min']) || isset($POST['lvl_max']))
    {
        if (isset($_POST['lvl_min']) && is_numeric($_POST['lvl_min']))
            $lvl_min = (int)$_POST['lvl_min'];
        else
            $lvl_min = 0;
        if (isset($_POST['lvl_max']) && is_numeric($_POST['lvl_max']))
            $lvl_max = (int)$_POST['lvl_max'];
        else
            $lvl_max = 99;

        $sql_search .= "AND level BETWEEN '$lvl_min' AND '$lvl_max'";
    }

    if (isset($_POST['dist_min']) || isset($POST['dist_max']))
    {
        if (isset($_POST['dist_min']) && is_numeric($_POST['dist_min']))
            $dist_min = (int)$_POST['dist_min'];
        else
            $dist_min = 0;
        if (isset($_POST['dist_max']) && is_numeric($_POST['dist_max']))
            $dist_max = (int)$_POST['dist_max'];
        else
            $dist_max = 99;
    }

    if (isset($_POST['tag_min']) || isset($POST['tag_max']))
    {
        if (isset($_POST['tag_min']) && is_numeric($_POST['tag_min']))
            $tag_min = (int)$_POST['tag_min'];
        else
            $tag_min = 0;
        if (isset($_POST['tag_max']) && is_numeric($_POST['tag_max']))
            $tag_max = (int)$_POST['tag_max'];
        else
            $tag_max = 99;
    }

    // Need to declare array here to use array_push then
    $profile_list = [];

    // Get user src tags.
    $src_tags = $PDO->query("SELECT id_tags FROM users WHERE username = '" . $_SESSION['auth']['username'] . "'")->fetchColumn();
    $src_tags = explode(',', $src_tags);

    $sqlq_profile_list = $PDO->query($sql_search);
    while ($search_profile_list_tmp = $sqlq_profile_list->fetch(PDO::FETCH_ASSOC))
    {
        // Get the distance between the user and the suggestion list.
        $position_dst_user = $PDO->query("SELECT lat, lon FROM users WHERE username = '" . $search_profile_list_tmp['username'] . "'")->fetch(PDO::FETCH_NUM);
        if (!empty($position_dst_user[0]) && !empty($position_dst_user[1]))
        {
            $dist = dist($position_cur_user[0], $position_cur_user[1], $position_dst_user[0], $position_dst_user[1], "K");
            $search_profile_list_tmp['dist'] = round($dist, 2);
        }
        // Get the number of commun tags between the user and the suggestion list.
        $dst_tags = $PDO->query("SELECT id_tags FROM users WHERE username = '" . $search_profile_list_tmp['username'] . "'")->fetchColumn();
        $dst_tags = explode(',', $dst_tags);
        $nb_tags = count(array_intersect($src_tags, $dst_tags));
        $search_profile_list_tmp['nb_tags'] = $nb_tags;
        $search_profile_list_tmp['lvl'] = $PDO->query("SELECT level FROM profile_stats WHERE id_user = (SELECT id FROM users WHERE username = '" . $search_profile_list_tmp['username'] . "')")->fetchColumn();

        // Get the list of users who blocks the dst user.
        $dst_block = $PDO->query("SELECT blocked_by FROM users WHERE username = '" . $search_profile_list_tmp['username'] . "'")->fetchColumn();
        $dst_block = explode(',', $dst_block);

        // Check if user info match with search post values
        if ($nb_tags >= $tag_min && $nb_tags <= $tag_max && round($dist, 2) >= $dist_min && round($dist, 2) <= $dist_max && $search_profile_list_tmp['username'] !== $_SESSION['auth']['username'] && !in_array($src_id, $dst_block))
            array_push($profile_list, $search_profile_list_tmp);
    }
    if (empty($profile_list))
    {
        echo "<center><div class='animated slideInDown error_search'> Your request is unavailable ! Please use valid intervals.</div></center>";
        header( "refresh:3;url=index.php" );
    }
}
else
{
    // Get back all near profiles / users
    $gender = $PDO->query("SELECT look_for FROM users INNER JOIN define_orientation ON define_orientation.orientation_gender_key = CONCAT(users.orientation, '_', users.gender)
    WHERE users.username = '" . $_SESSION['auth']['username'] . "'")->fetchColumn();

    $orientation = $PDO->query("SELECT users.id FROM users INNER JOIN define_orientation ON define_orientation.orientation_gender_key = CONCAT(users.orientation, '_', users.gender)
    WHERE users.username != '" . $_SESSION['auth']['username'] . "'
    AND look_for LIKE CONCAT('%', (SELECT gender FROM users WHERE username = '" . $_SESSION['auth']['username'] . "'), '%' )")->fetchAll(PDO::FETCH_COLUMN);

    $gender = explode(',', $gender);$gender = join("','", $gender);
    $orientation = implode(',', $orientation);

    $sql_people_near =
    "SELECT users.id, username, gender, prez, age, bio FROM users
    WHERE gender IN ('$gender') AND id IN ($orientation) AND username != '" . $_SESSION['auth']['username'] . "'";

    // Need to declare array here to use array_push then
    $profile_list = [];

    $src_tags = $PDO->query("SELECT id_tags FROM users WHERE username = '" . $_SESSION['auth']['username'] . "'")->fetchColumn();
    $src_tags = explode(',', $src_tags);

    $sqlr_people_near = $PDO->query($sql_people_near);
    while ($profile_list_tmp = $sqlr_people_near->fetch(PDO::FETCH_ASSOC))
    {
        // Get the distance between the user and the suggestion list.
        $position_dst_user = $PDO->query("SELECT lat, lon FROM users WHERE username = '" . $profile_list_tmp['username'] . "'")->fetch(PDO::FETCH_NUM);
        if (!empty($position_dst_user[0]) && !empty($position_dst_user[1]))
        {
            $dist = dist($position_cur_user[0], $position_cur_user[1], $position_dst_user[0], $position_dst_user[1], "K");
            $profile_list_tmp['dist'] = (int)round($dist, 2);
        }
        // Get the number of commun tags between the user and the suggestion list.
        $dst_tags = $PDO->query("SELECT id_tags FROM users WHERE username = '" . $profile_list_tmp['username'] . "'")->fetchColumn();
        $dst_tags = explode(',', $dst_tags);
        $nb_tags = count(array_intersect($src_tags, $dst_tags));
        $profile_list_tmp['nb_tags'] = (int)$nb_tags;
        $profile_list_tmp['age'] = (int)$profile_list_tmp['age'];
        $profile_list_tmp['lvl'] = (int)$PDO->query("SELECT level FROM profile_stats WHERE id_user = (SELECT id FROM users WHERE username = '" . $profile_list_tmp['username'] . "')")->fetchColumn();

        // Get the list of users who blocks the dst user.
        $dst_block = $PDO->query("SELECT blocked_by FROM users WHERE username = '" . $profile_list_tmp['username'] . "'")->fetchColumn();
        $dst_block = explode(',', $dst_block);

        if (!in_array($src_id, $dst_block))
            array_push($profile_list, $profile_list_tmp);
    }
}

// var_dump($profile_list);

// Print list of profile's people near user
$people_near_from_u =
<body >
    <div class="page_content">
        <x:js-scope>
            <People-near profiles_near={$profile_list}
                         src_username={$_SESSION['auth']['username']} />
        </x:js-scope>
    </div>
</body>
;


echo $people_near_from_u;
