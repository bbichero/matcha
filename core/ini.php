<?hh

/*
## Error type possible for Auth_alert class
- intern
- bad_paswd
- unknow_user
- password_miss_match
- know_email
- know_user
*/

// Set the hash function.
ini_set('session.hash_function', "sha512");
// How many bits per character of the hash.
// The possible values are '4' (0-9, a-f), '5' (0-9, a-v), and '6' (0-9, a-z, A-Z, "-", ",").
ini_set('session.hash_bits_per_character', 5);
// Force the session to only use cookies, not URL variables.
ini_set('session.use_only_cookies', 1);
// Disabled display of session ID
ini_set('session.use_trans_sid', false);

// Don't allow uninitialized session_id
ini_set("session.use_strict_mode", 1);

// Prevents cookies stolen by JavaScript injection
//$httpOnly = true;

// Get cookie parameters
//$session = session_get_cookie_params();

// Set cookie parameters
//session_set_cookie_params(1000, $session['path'], "matcha", false, $httpOnly);

// Unset $_SESSION
unset($_SESSION);

// Start session
session_start();

// Clean var in $_SESSION
if (isset($_SESSION['auth']['username']))
    $_SESSION['auth']['username'] = addslashes($_SESSION['auth']['username']);
if (isset($_SESSION['auth']['session_id']))
    $_SESSION['auth']['session_id'] = addslashes($_SESSION['auth']['session_id']);

// Mandatory include for database connection
require(__DIR__ . "/../vendor/autoload.php");
require(__DIR__ . "/../vendor/facebook/xhp-lib/init.php");
require(__DIR__ . "/../class/hack/MyQuery.php");
require(__DIR__ . "/../class/hack/MyPool.php");
require(__DIR__ . "/config.php");
require(__DIR__ . "/../position_tool.php");

// Include Connection class (TMP) TODO : remove in futur
require(__DIR__ . "/../class/hack/Connection.class.php");

// show all error
error_reporting(E_ALL);
ini_set('display_errors', 1);

// End of tags.
$html_closure =
"</html>";

// Set error handler to print error in browser
set_error_handler(function ($errorNumber, $message, $errfile, $errline)
{
    switch ($errorNumber)
    {
        case E_ERROR :
        $errorLevel = 'Error';
        break;

        case E_WARNING :
        $errorLevel = 'Warning';
        break;

        case E_NOTICE :
        $errorLevel = 'Notice';
        break;

        default :
        $errorLevel = 'Undefined';
    }

    echo "<br/><b>$errorLevel</b>: $message in <b>
    $errfile </b> on line <b>$errline </b><br/>";
});


/**
* Ensures an ip address is both a valid IP and does not fall within
* a private network range.
*/
function validate_ip($ip)
{
    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) === false)
    return false;
    return true;
}

/*
*  Initialise connection with MyPool and MyQuery class
*  MyPool permit the connetion to the Mysql database
*  MyQuery permit to access at some query method like query() or queryf()
*  And each query can be log or not following parameters enter in this function
*/
// async function initialise_connection($host_name, int $port, $db_name, $username,
// $password, $log_request, $log_request_type): Awaitable<MyQuery>
// {
//     $MyPool = new MyPool();
//     $conn = await $MyPool->connect($host_name, $port, $db_name, $username, $password);
//     $connection = new MyQuery($conn, $log_request, $log_request_type);
//     return ($connection);
// }

// Create connection object that will be available in all page
//$connection = \HH\asio\join(initialise_connection($host_name, $port, $db_name, $username,
//                                    $password, $log_request, $log_request_type));

// Instantiate the PDO object for connection TODO : remove in futur
$PDO = new Connection($host_name, $db_name, $username, $password, $log_request, $debug_req, $option_pdo);

// Set PDO attribute
$PDO->setUsername = "No log user";


// Check Sign up POST
if (isset($_POST['sign_up']) && !empty($_POST['sign_up_email']) && !empty($_POST['sign_up_username']))
{
    $uip_sign_up_email = $_POST['sign_up_email'];
    $uip_sign_up_username = $_POST['sign_up_username'];
    $error_type = null;
    $auth_type = "sign_up";
}
else
{
    $uip_sign_up_email = null;
    $uip_sign_up_username = null;
    $error_type = null;
    $auth_type = "sign_in";
}

// Create Auth form class (sign-up and sign-in)
class :auth-form extends :x:element
{
    use XHPHelpers;
    use XHPReact;

    attribute
    :xhp:html-element,
    string sign_up_username @required,
    string sign_up_email @required,
    string auth_type @required,
    string reset_form @required;

    protected function render(): XHPRoot
    {
        $this->constructReactInstance(
        'Auth_form',
        Map {
                'sign_up_username' => $this->:sign_up_username,
                'sign_up_email' => $this->:sign_up_email,
                'auth_type' => $this->:auth_type,
                'reset_form' => $this->:reset_form,
            }
        );
        return <div id={$this->getID()} />;
    }
}

// Create Auth alert class for sign-up and sign-in
class :auth-alert extends :x:element
{
    use XHPHelpers;
    use XHPReact;

    attribute
    :xhp:html-element,
    string auth_type @required,
    string error_type @required;

    protected function render(): XHPRoot
    {
        $this->constructReactInstance(
        'Auth_alert',
        Map {
                'auth_type' => $this->:auth_type,
                'error_type' => $this->:error_type,
            }
        );
        return <div id={$this->getID()} />;
    }
}

class :notif-counter extends :x:element
{
    use XHPHelpers;
    use XHPReact;

    attribute
    :xhp:html-element,
    int count @required,
    string src_username @required;

    protected function render(): XHPRoot
    {
        $this->constructReactInstance(
        'Notif_counter',
            Map {
                'count' => $this->:count,
                'src_username' => $this->:src_username,
            }
        );
        return <div id={$this->getID()} />;
    }
}

if (isset($_POST["reset"]))
{
    // Mail checking.
    if (filter_var($_POST["reset_email"], FILTER_VALIDATE_EMAIL))
        $email = $_POST["reset_email"];
    else
    {
        echo "email is not valid";
        exit;
    }
    $req = $PDO->prepare('SELECT email FROM users WHERE email = :email', array('email' => $email));
    $userExists = $req->fetch(PDO::FETCH_ASSOC);
    $PDO = null;

    // Request succed if the usermail is in the database.
    if ($email = $userExists["email"])
    {
        // Secure the password request.
        $secu     = "498#2D83B631%3800EBD!801600D*7E3CC13";
        $password = hash('sha512', $secu.$userExists["email"]);
        $link     = "matcha.bbichero.com$url_path?auth=".$password;

        // Mail preparation.
        $body   = "Hi friend ! It appears that you have requested a password reset for your Matcha account.\n\nTo reset your password, please click the link below.\n\n" . $link . "";
        $sujet  = "Matcha: Password Reset";
        $header = "From: \"Matcha\"<matcha@42.fr>" . "\r\n";
        $header.= "Reply-to: \"Matcha\" <matcha@42.fr>" . "\r\n";
        $header.= "MIME-Version: 1.0" . "\r\n";
        $header.= 'X-Mailer: PHP/' . phpversion();
        mail($email, $sujet, $body, $header);

        echo '<div style="
            text-align: center;
            margin: 20px auto;
            width: 343px;
            max-height:80%;
            padding:4px;
            font-size: 15px;
            text-align: center;
            -webkit-border-radius: 8px/7px;
            -moz-border-radius: 8px/7px;
            border-radius: 8px/7px;
            background-color: #ebebeb;
            -webkit-box-shadow: 1px 2px 5px rgba(0,0,0,.31);
            -moz-box-shadow: 1px 2px 5px rgba(0,0,0,.31);
            box-shadow: 1px 2px 5px rgba(0,0,0,.31);
            border: solid 1px #cbc9c9;">Your password recovery key has been sent to your e-mail address.</div>';
    }
    // Error case of non valid email.
    else
        echo '<div style="
            text-align: center;
            margin: 20px auto;
            width: 343px;
            max-height:80%;
            padding:4px;
            font-size: 15px;
            text-align: center;
            -webkit-border-radius: 8px/7px;
            -moz-border-radius: 8px/7px;
            border-radius: 8px/7px;
            background-color: #ebebeb;
            -webkit-box-shadow: 1px 2px 5px rgba(0,0,0,.31);
            -moz-box-shadow: 1px 2px 5px rgba(0,0,0,.31);
            box-shadow: 1px 2px 5px rgba(0,0,0,.31);
            border: solid 1px #cbc9c9;">No user with that e-mail address exists.</div>';
}

if (isset($_POST["reset_key"]))
{
    // Get the users values and check the security of the password.
    $email = $_POST["reset_key_email"];
    $password = $_POST["reset_password"];
    $confirmpassword = $_POST["reset_password_confirm"];
    $hash = $_POST["q"];
    if (preg_match("/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/", $password))
    {
        $salt = "498#2D83B631%3800EBD!801600D*7E3CC13";
        $resetkey = hash('sha512', $salt.$email);

        // Check if the secure key matchs to the key sent by mail to the user.
        if ($resetkey == $hash)
        {
            if ($password == $confirmpassword)
            {
                $password = hash('sha512', $password);

                // If the password is regular, send a success request and update the password in the database.
                $req = $PDO->prepare('UPDATE users SET password = :password WHERE email = :email', array('password' => $password, 'email' => $email));
                $PDO = null;
                echo '<div style="
                    text-align: center;
                    margin: 20px auto;
                    width: 343px;
                    max-height:80%;
                    padding:4px;
                    font-size: 15px;
                    text-align: center;
                    -webkit-border-radius: 8px/7px;
                    -moz-border-radius: 8px/7px;
                    border-radius: 8px/7px;
                    background-color: #ebebeb;
                    -webkit-box-shadow: 1px 2px 5px rgba(0,0,0,.31);
                    -moz-box-shadow: 1px 2px 5px rgba(0,0,0,.31);
                    box-shadow: 1px 2px 5px rgba(0,0,0,.31);
                    border: solid 1px #cbc9c9;">Your password has been successfully reset.</div>';
            }
            // Print an error if passwords are different/
            else
                echo '<div style="
                    text-align: center;
                    margin: 20px auto;
                    width: 343px;
                    max-height:80%;
                    padding:4px;
                    font-size: 15px;
                    text-align: center;
                    -webkit-border-radius: 8px/7px;
                    -moz-border-radius: 8px/7px;
                    border-radius: 8px/7px;
                    background-color: #ebebeb;
                    -webkit-box-shadow: 1px 2px 5px rgba(0,0,0,.31);
                    -moz-box-shadow: 1px 2px 5px rgba(0,0,0,.31);
                    box-shadow: 1px 2px 5px rgba(0,0,0,.31);
                    border: solid 1px #cbc9c9;">Your password do not match.</div>';
        }
        // Print an error if the two security keys are different.
        else
            echo '<div style="
                text-align: center;
                margin: 20px auto;
                width: 343px;
                max-height:80%;
                padding:4px;
                font-size: 15px;
                text-align: center;
                -webkit-border-radius: 8px/7px;
                -moz-border-radius: 8px/7px;
                border-radius: 8px/7px;
                background-color: #ebebeb;
                -webkit-box-shadow: 1px 2px 5px rgba(0,0,0,.31);
                -moz-box-shadow: 1px 2px 5px rgba(0,0,0,.31);
                box-shadow: 1px 2px 5px rgba(0,0,0,.31);
                border: solid 1px #cbc9c9;">Your password reset key is invalid.</div>';
    }
    else
        echo '<div style="
            text-align: center;
            margin: 20px auto;
            width: 343px;
            max-height:80%;
            padding:4px;
            font-size: 15px;
            text-align: center;
            -webkit-border-radius: 8px/7px;
            -moz-border-radius: 8px/7px;
            border-radius: 8px/7px;
            background-color: #ebebeb;
            -webkit-box-shadow: 1px 2px 5px rgba(0,0,0,.31);
            -moz-box-shadow: 1px 2px 5px rgba(0,0,0,.31);
            box-shadow: 1px 2px 5px rgba(0,0,0,.31);
            border: solid 1px #cbc9c9;">Your password is not well formated.</div>';
}

// Check Sing in POST, check if value aren't empty
if (isset($_POST['sign_in']) && isset($_POST['sign_in_username']) && !empty($_POST['sign_in_username'])
&& isset($_POST['sign_in_password']) && !empty($_POST['sign_in_password']))
{
    // Check username existance in DB
    $sql_user = "SELECT username, password FROM users WHERE BINARY username = :username";
    $sqlq_params = array("username" => $_POST['sign_in_username']);

    $sqlr_user = $PDO->prepare($sql_user, $sqlq_params)->fetch(PDO::FETCH_ASSOC);

    if (!empty($sqlr_user['username']))
    {
        // Hash password from POST
        $uig_sign_in_password_hash = hash('sha512', $_POST['sign_in_password']);

        // Check if password in POST match with password in DB for concern username
        if ($uig_sign_in_password_hash === $sqlr_user['password'])
        {
            // Clean result query variable
            //unset($sqlr_user);

            // Regenerate session_id
            // session_regenerate_id(true);

            // Set new $_SESSION value
            $_SESSION['auth']['username'] = $_POST['sign_in_username'];
            $_SESSION['auth']['session_id'] = session_id();

            $sqlq_session_id = "UPDATE users SET session_id = :session_id WHERE BINARY username = :username";
            $sqlq_params = array('session_id' => session_id(), 'username' => $_POST['sign_in_username']);

            if (empty($PDO->query("SELECT complete FROM users WHERE username = '" . $sqlr_user['username'] .  "'")->fetchColumn()))
            {
                $check_complete = $PDO->query("SELECT username, first_name, last_name, prez, alt1, alt2, bio, age, phone, gender, orientation, city FROM users WHERE username = '" . $sqlr_user['username'] .  "'");

                $i = 0;
                $check_one = $check_complete->fetch(PDO::FETCH_NUM);
                while (isset($check_one[$i]))
                {
                    if (empty($check_one[$i]))
                        break;
                    $i++;
                }
                if (!isset($check_one[$i]))
                {
                    $user_id = $PDO->query("SELECT id FROM users WHERE username = '" . $sqlr_user['username'] .  "'")->fetchColumn();
                    $PDO->query("UPDATE users SET complete = 'Y' WHERE username = '" . $sqlr_user['username'] .  "'");
                    $PDO->query("UPDATE profile_stats SET xp = xp + 1000  WHERE id_user = '$user_id'");
                }
            }
            // Update DB with new value
            if (!$PDO->prepare($sqlq_session_id, $sqlq_params))
            {
                // Internal error
                // Set error type
                $error_type = "intern";
            }
        }
        else
        {
            // Invalid password
            // Set error type
            $error_type = "bad_paswd";
        }
    }
    else
    {
        // Username not exist
        // Set error type
        $error_type = "unknow_user";
    }
}
else if (isset($_POST['sign_up']) && isset($_POST['sign_up_username']) && isset($_POST['sign_up_email'])
&& isset($_POST['sign_up_password']) && isset($_POST['sign_up_password_confirm']))
{
    // Check existance of username
    $user_params = array('username' => $_POST['sign_up_username']);
    $username_exist = $PDO->prepare("SELECT username FROM users WHERE username = :username", $user_params)->fetchColumn();

    // If username exist, print error, else process
    if (empty($username_exist))
    {
        // Check existance of email
        $email_params = array('email' => $_POST['sign_up_email']);
        $email_exist = $PDO->prepare("SELECT email FROM users WHERE email = :email", $email_params)->fetchColumn();

        // If email exist, print error, else process
        if (empty($email_exist))
        {
            // Check if 2 password match, if not print error else create his account in DB and connect him
            if ($_POST['sign_up_password'] === $_POST['sign_up_password_confirm'])
            {
                // Hash password
                $hash_password = hash('sha512', $_POST['sign_up_password']);

                // Insert into user table new user
                $new_user_params = array(
                    'username' => $_POST['sign_up_username'],
                    'first_name' => $_POST['sign_up_first_name'],
                    'last_name' => $_POST['sign_up_last_name'],
                    'email' => $_POST['sign_up_email'],
                    'password' => $hash_password,
                );
                $new_user_sql =
                "INSERT INTO users (username, first_name, last_name, email, password, last_signin_date, creation_date, lupdate_date)
                VALUES (:username, :first_name, :last_name, :email, :password, now(), now(), now())";
                $new_user_id = $PDO->req_last_insert("prepare", $new_user_sql, $new_user_params);

                $new_user_stats_sql =
                "INSERT INTO profile_stats (id_user, level, xp, percent_xp, creation_date, lupdate_date)
                VALUES ('$new_user_id', '0', '0', '0', now(), now())";
                $PDO->query($new_user_stats_sql, $new_user_params);

                // Regenerate session_id
                // session_regenerate_id(true);

                // Set new $_SESSION value
                $_SESSION['auth']['username'] = $_POST['sign_up_username'];
                $_SESSION['auth']['session_id'] = session_id();

                $sqlq_session_id = "UPDATE users SET session_id = :session_id WHERE username = :username";
                $sqlq_params = array('session_id' => session_id(), 'username' => $_POST['sign_up_username']);

                // Update session_id
                $PDO->prepare($sqlq_session_id, $sqlq_params);
            }
            else
            {
                // Email already exist
                // Set error type
                $error_type = "password_miss_match";
            }
        }
        else
        {
            // Email already exist
            // Set error type
            $error_type = "know_email";
        }
    }
    else
    {
        // Username already exist
        // Set error type
        $error_type = "know_user";
    }

    // Clean query variable
    unset($username_exist);
    unset($email_exist);
}
if (isset($_GET['auth']))
    $reset_form = $_GET['auth'];
else
    $reset_form = "";

// Stock auth form class (sign-in and sign-up)
$auth_form =
<body>
    <div class="auth_alert">
        <x:js-scope>
            <auth-alert error_type={$error_type}
                auth_type={$auth_type} />
        </x:js-scope>
    </div>
    <div class="auth_form animated slideInLeft">
        <form name="auth" class="center" method="post">
            <x:js-scope>
                <auth-form  sign_up_username={$uip_sign_up_username}
                            sign_up_email={$uip_sign_up_email}
                            auth_type={$auth_type}
                            reset_form={$reset_form} />
            </x:js-scope>
        </form>
    </div>
</body>;



// Check value in $_SESSION var
if (!empty($_SESSION['auth']['username']) && !empty($_SESSION['auth']['session_id']))
{
    $sqlq_session_id = "SELECT session_id FROM users WHERE username = :username";
    $sqlq_params = array("username" => $_SESSION['auth']['username']);

    // Get back session_id in DB corresponding to username in $_SESSION
    $session_id_db = $PDO->prepare($sqlq_session_id, $sqlq_params)->fetchColumn();

    // Session id in cookie
    $session_id_cookie = session_id();

    // Check if username and session_id in $_SESSION match with username and session_id in DB
    // Check if session_id in browser match with session_id in $_SESSION
    if ($session_id_cookie === $_SESSION['auth']['session_id'] && $session_id_db === $_SESSION['auth']['session_id'])
    {
        // Save username and regenerate session_id
        $username = $_SESSION['auth']['username'];
        $PDO->setUsername($username);
        session_regenerate_id(true);

        // Get back IP addr of user
        $user_ip = get_ip_address();
        $user_json = get_json_location_user($user_ip);
        $user_json_decode = json_decode($user_json, true);

        // Set new $_SESSION value
        $_SESSION['auth']['username'] = $username;
        $_SESSION['auth']['session_id'] = session_id();

        if ($PDO->query("SELECT position_state FROM users WHERE username = '" . $_SESSION['auth']['username'] . "'")->fetchColumn() != 'A')
        {
            $sqlq_session_id = "UPDATE users SET session_id = :session_id, city = :city, lat = :lat, lon = :lon WHERE username = :username";
            $sqlq_params = array('session_id' => session_id(), 'city' => $user_json_decode['city'], 'lat' => $user_json_decode['lat'], 'lon' => $user_json_decode['lon'], 'username' => $username);
        }
        else
        {
            $sqlq_session_id = "UPDATE users SET session_id = :session_id WHERE username = :username";
            $sqlq_params = array('session_id' => session_id(), 'username' => $username);
        }

        // Update DB with new value
        if ($PDO->prepare($sqlq_session_id, $sqlq_params))
        {
            // Process page
            // Print header
            if (!isset($output))
            require("header.php");

            $req = $PDO->query("SELECT users.id, username, prez, percent_xp, level FROM users LEFT OUTER JOIN profile_stats ON users.id = profile_stats.id_user WHERE username = '" . $_SESSION['auth']['username'] . "'");
            $user_data = $req->fetch(PDO::FETCH_ASSOC);

            // Pex system for checking the status.
            $user_id  = $PDO->query("SELECT id FROM users WHERE username = '" . $_SESSION['auth']['username'] . "'")->fetchColumn();
            $xp_count = $PDO->query("SELECT xp FROM profile_stats WHERE id = '$user_id'")->fetchColumn();
            $arr = explode('.', round(0.09 * sqrt($xp_count), 2)); // Formula to transform xp points in lvl and get lvl and percetage for the DB update.
            $lvl = $arr[0];
            if (isset($arr[1]))
                $percent = $arr[1];
            else
                $percent = 0;
            $PDO->query("UPDATE profile_stats SET level = $lvl, percent_xp = $percent WHERE id = $user_id");

            // Set user id in COnnection class
            $PDO->setIdUser($user_data['id']);
            $xp = "width: " . $user_data['percent_xp'] . "%";

            // Stock of omnibar
            $count_id = (int)$PDO->query("SELECT COUNT(id) FROM notifications WHERE seen != 'Y' AND id_user_dst =  '$user_id'")->fetchColumn();
            $omnibar =
            <div class="bar">
                <div>
                    <a href={$url_path . "/index.php"}> <img class="home" src="../resources/home.png"></img></a>
                    <a href={$url_path . "/messages.php"}> <img class="notif" src="../resources/message.png"></img></a>
                </div>
                <div>
                    <img class="pp" src={($user_data['prez']) ? $user_data['prez'] : "../resources/empty_profile.png"} alt="profile_pic"></img>
                </div>
                <div class="right_panel">
                    <a style="border: 1px solid black;" class="pure-button" href={$url_path . "/user_settings.php"}  name="settings">
                    <x:js-scope>
                        <notif-counter  count={$count_id}
                                        src_username={$_SESSION['auth']['username']}
                            />
                    </x:js-scope>
                    </a><br></br><br></br>
                    <a style="border: 1px solid black;" class="pure-button" href={$url_path . "/log_out.php"}  name="logout">Logout  </a>
                </div>
                <div class="username"> {$user_data['username']} </div>
                <div class="container">
                    <div class="filled animated slideInLeft" style={$xp}></div>
                    <div class="pex">Level: {$user_data['level']} - {$user_data['percent_xp']}%</div>
                </div>
            </div>;
            // Print user bar progression / setting
            // Print navigation bar
            if ($page_name !== "user_settings" && !isset($output))
                echo $omnibar;
        }
        // Process page
        else
        {
            require("header.php");
            echo "Cant update session id in databases";
            exit($auth_form . $html_closure);
            // Print footer
        }
    }
    else
    {
        require("header.php");
        echo "pas bon session id";
        exit($auth_form . $html_closure);
        // Print footer
    }
}
else
{
    require("header.php");
    exit($auth_form . $html_closure);
    // Print footer
}
