<?hh

$page_name = "user_settings";

// require mandatody page
require("core/ini.php");

// Create user infomation div in top of settings page
class :User_info_top extends :x:element
{
    use XHPHelpers;
    use XHPReact;

    attribute
    :xhp:html-element,
    array item_edit_list @required,
    array  notif_list@required,
    string url_path @required,
    string resources_path @required,
    string username @required,
    string percent_xp @required,
    string xp @required,
    string level @required,
    string message @required,
    string prez @required,
    string src_prez @required,
    string alt1 @required,
    string alt2 @required,
    string city @required,
    string lat @required,
    string lon @required,
    string src_username @required,
    array  tags @required,
    array  suggestions @required;

    protected function render(): XHPRoot
    {
        $this->constructReactInstance(
        'User_info_top',
            Map {
                'item_edit_list' => $this->:item_edit_list,
                'notif_list' => $this->:notif_list,
                'url_path' => $this->:url_path,
                'resources_path' => $this->:resources_path,
                'username' => $this->:username,
                'percent_xp' => $this->:percent_xp,
                'xp' => $this->:xp,
                'level' => $this->:level,
                'message' => $this->:message,
                'prez' => $this->:prez,
                'src_prez' => $this->:src_prez,
                'alt1' => $this->:alt1,
                'alt2' => $this->:alt2,
                'city' => $this->:city,
                'lat' => $this->:lat,
                'lon' => $this->:lon,
                'src_username' => $this->:src_username,
                'tags' => $this->:tags,
                'suggestions' => $this->:suggestions,
            }
        );
        return <div id={$this->getID()} />;
    }
}


// Constantes
define('TARGET', 'resources/');    // Repertoire cible
define('MAX_SIZE', 100000);    // Taille max en octets du fichier
define('WIDTH_MAX', 800);    // Largeur max de l'image en pixels
define('HEIGHT_MAX', 800);    // Hauteur max de l'image en pixels

// Tableaux de donnees
$tabExt = array('jpg','gif','png','jpeg');    // Extensions autorisees
$infosImg = array();

// Variables
$extension = '';
$message = '';
$nomImage = '';

if (isset($_FILES['upload']['name']))
{
    $extension  = pathinfo($_FILES['upload']['name'], PATHINFO_EXTENSION);
    $tmp_up = $_FILES['upload']['tmp_name'];


    if (in_array(strtolower($extension),$tabExt))
    {
        $infosImg = getimagesize($_FILES['upload']['tmp_name']);
        if ($infosImg[2] >= 1 && $infosImg[2] <= 14)
        {
            // if (($infosImg[0] <= WIDTH_MAX) && ($infosImg[1] <= HEIGHT_MAX) && (filesize($_FILES['upload']['tmp_name']) <= MAX_SIZE))
            // {
                if (isset($_FILES['upload']['error']) && UPLOAD_ERR_OK === $_FILES['upload']['error'])
                {
                    $id_usr = $PDO->query("SELECT id FROM users WHERE username = '" . $_SESSION['auth']['username'] . "'")->fetchColumn();
                    $data = file_get_contents($tmp_up);
                    $base64 = 'data:image/' . $extension . ';base64,' . base64_encode($data);
                    $PDO->query("UPDATE users SET prez = '" . $base64 . "', active = 'Y' WHERE username = '" . $_SESSION['auth']['username'] . "'");
                    $PDO->query("UPDATE profile_stats SET xp = xp + 1000  WHERE id_user = '$id_usr'");
                }
                else
                    $message = 'An internal error stopped the uplaod.';
            // }
            // else
            //     $message = 'Size error !';
        }
        else
            $message = 'The file is not an image !';
    }
    else
        $message = 'Invalid extension !';
}

if (isset($_FILES['upload_l']['name']))
{
    $extension  = pathinfo($_FILES['upload_l']['name'], PATHINFO_EXTENSION);
    $tmp_up = $_FILES['upload_l']['tmp_name'];


    if (in_array(strtolower($extension),$tabExt))
    {
        $infosImg = getimagesize($_FILES['upload_l']['tmp_name']);
        if ($infosImg[2] >= 1 && $infosImg[2] <= 14)
        {
            // if (($infosImg[0] <= WIDTH_MAX) && ($infosImg[1] <= HEIGHT_MAX) && (filesize($_FILES['upload']['tmp_name']) <= MAX_SIZE))
            // {
                if (isset($_FILES['upload_l']['error']) && UPLOAD_ERR_OK === $_FILES['upload_l']['error'])
                {
                    $id_usr = $PDO->query("SELECT id FROM users WHERE username = '" . $_SESSION['auth']['username'] . "'")->fetchColumn();
                    $data = file_get_contents($tmp_up);
                    $base64 = 'data:image/' . $extension . ';base64,' . base64_encode($data);
                    $PDO->query("UPDATE users SET alt1 = '" . $base64 . "' WHERE username = '" . $_SESSION['auth']['username'] . "'");
                    $PDO->query("UPDATE profile_stats SET xp = xp + 500  WHERE id_user = '$id_usr'");
                }
                else
                    $message = 'An internal error stopped the uplaod.';
            // }
            // else
            //     $message = 'Size error !';
        }
        else
            $message = 'The file is not an image !';
    }
    else
        $message = 'Invalid extension !';
}

if (isset($_FILES['upload_r']['name']))
{
    $extension  = pathinfo($_FILES['upload_r']['name'], PATHINFO_EXTENSION);
    $tmp_up = $_FILES['upload_r']['tmp_name'];


    if (in_array(strtolower($extension),$tabExt))
    {
        $infosImg = getimagesize($_FILES['upload_r']['tmp_name']);
        if ($infosImg[2] >= 1 && $infosImg[2] <= 14)
        {
            // if (($infosImg[0] <= WIDTH_MAX) && ($infosImg[1] <= HEIGHT_MAX) && (filesize($_FILES['upload']['tmp_name']) <= MAX_SIZE))
            // {
                if (isset($_FILES['upload_r']['error']) && UPLOAD_ERR_OK === $_FILES['upload_r']['error'])
                {
                    $id_usr = $PDO->query("SELECT id FROM users WHERE username = '" . $_SESSION['auth']['username'] . "'")->fetchColumn();
                    $data = file_get_contents($tmp_up);
                    $base64 = 'data:image/' . $extension . ';base64,' . base64_encode($data);
                    $PDO->query("UPDATE users SET alt2 = '" . $base64 . "' WHERE username = '" . $_SESSION['auth']['username'] . "'");
                    $PDO->query("UPDATE profile_stats SET xp = xp + 500  WHERE id_user = '$id_usr'");
                }
                else
                    $message = 'An internal error stopped the uplaod.';
            // }
            // else
            //     $message = 'Size error !';
        }
        else
            $message = 'The file is not an image !';
    }
    else
        $message = 'Invalid extension !';
}
// else // Sinon on affiche une erreur pour le champ vide
// {
//     $message = 'Veuillez remplir le formulaire svp !';
//     $img_form =
//     <div>
//         <div className="user_picture">
//             <form enctype="multipart/form-data" method="post">
//                 <input name="fichier" type="file" id="fichier_a_uploader" />
//                 <input type="submit" name="submit" value="Upload" />
//             </form>
//         </div>
//     </div>;
//
//     echo $img_form;
// }


// Get back all near profiles / users
$sql_user_info =
"SELECT username, first_name, last_name, age, bio, percent_xp, xp, level, gender, orientation, email, phone, prez, alt1, alt2, city, lon, lat
FROM users
LEFT OUTER JOIN profile_stats ON users.id = profile_stats.id_user
WHERE username = '" . $_SESSION['auth']['username'] . "'";

// LEFT OUTER JOIN img_profile ON users.id = img_profile.id_user



$user_infos = $PDO->query($sql_user_info)->fetch(PDO::FETCH_ASSOC);
if (!empty($id_tags = $PDO->query("SELECT id_tags FROM users WHERE username = '" . $_SESSION['auth']['username'] . "'")->fetchColumn())); else $id_tags = null;;
if ($suggestions = $PDO->query("SELECT id, name FROM tags")->fetchAll(PDO::FETCH_ASSOC)); else $suggestions = null;;

if ($id_tags !== null)
    $tags = $PDO->query("SELECT name FROM tags WHERE id IN ($id_tags)")->fetchAll(PDO::FETCH_ASSOC);
else
    $tags = null;
$src_prez = $PDO->query("SELECT prez FROM users WHERE username = '" . $_SESSION['auth']['username'] . "'")->fetchColumn();

$id_usr = $PDO->query("SELECT id FROM users WHERE username = '" . $_SESSION['auth']['username'] . "'")->fetchColumn();
$PDO->query("UPDATE notifications SET seen = 'Y' WHERE id_user_dst = '$id_usr'");
$notif_list = $PDO->query("SELECT id_user_src, id_user_dst, content, seen, username, prez FROM notifications INNER JOIN users ON notifications.id_user_src = users.id WHERE id_user_dst = '$id_usr' ORDER BY notifications.creation_date DESC LIMIT 12")->fetchAll(PDO::FETCH_ASSOC);

// var_dump($notif_list);

// Print user information on top of page (xp bar, image profile, etc ...)
$user_info_top =
<body>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAVPpBdLg4Z5d8qSJMi6nCoRt_JvzSR4Vk"></script>
    <div>
        <x:js-scope>
            <User_info_top  item_edit_list={
                                array(
                                    array("name" => "gender", "value" => $user_infos['gender']),
                                    array("name" => "orientation", "value" => $user_infos['orientation']),
                                    array("name" => "first_name", "value" => $user_infos['first_name']),
                                    array("name" => "age", "value" => $user_infos['age']),
                                    array("name" => "bio", "value" => $user_infos['bio']),
                                    array("name" => "last_name", "value" => $user_infos['last_name']),
                                    array("name" => "email", "value" => $user_infos['email']),
                                    array("name" => "phone", "value" => $user_infos['phone']),
                                    array("name" => "city", "value" => $user_infos['city']))}
                            resources_path={"resources"}
                            notif_list={$notif_list}
                            url_path={$url_path}
                            username={$user_infos['username']}
                            percent_xp={$user_infos['percent_xp']}
                            xp={$user_infos['xp']}
                            level={$user_infos['level']}
                            message={$message}
                            src_prez={$src_prez}
                            prez={$user_infos['prez']}
                            alt1={$user_infos['alt1']}
                            alt2={$user_infos['alt2']}
                            city={$user_infos['city']}
                            lat={$user_infos['lat']}
                            lon={$user_infos['lon']}
                            tags={$tags}
                            suggestions={$suggestions}
                            src_username={$_SESSION['auth']['username']}
            />
        </x:js-scope>
    </div>
</body>;

echo $user_info_top . $html_closure;
