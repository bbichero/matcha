<?hh

$page_name = "messages";

// require(__DIR__ . "/vendor/autoload.php");
// require(__DIR__ . "/vendor/facebook/xhp-lib/init.php");
// require(__DIR__ . "/class/hack/MyQuery.php");
// require(__DIR__ . "/class/hack/MyPool.php");
// require(__DIR__ . "/core/config.php");
// require(__DIR__ . "/class/hack/Connection.class.php");
require("core/ini.php");

// Create Messages list class
class :Conversation extends :x:element
{
    use XHPHelpers;
    use XHPReact;

    attribute
    :xhp:html-element,
    array messages_list @required,
    string dst_username @required,
    string id_dst_username @required,
    string src_username @required,
    string id_src_username @required,
    string profile_prez @required;

    protected function render(): XHPRoot
    {
        $this->constructReactInstance(
        'Conversation',
            Map {
                'messages_list' => $this->:messages_list,
                'dst_username'  => $this->:dst_username,
                'id_dst_username'  => $this->:id_dst_username,
                'src_username'  => $this->:src_username,
                'id_src_username'  => $this->:id_src_username,
                'profile_prez'  => $this->:profile_prez,
            }
        );
        return <div id={$this->getID()} />;
    }
}

// Create Matches list class
class :Matches extends :x:element
{
    use XHPHelpers;
    use XHPReact;

    attribute
    :xhp:html-element,
    string profile_prez @required,
    int    id_matched   @required,
    string user_matched @required,
    string user_src     @required;

    protected function render(): XHPRoot
    {
        $this->constructReactInstance(
        'Matches',
            Map {
                'profile_prez' => $this->:profile_prez,
                'id_matched'   => $this->:id_matched,
                'user_matched' => $this->:user_matched,
                'user_src'     => $this->:user_src,
            }
        );
        return <div id={$this->getID()} />;
    }
}

// Create Conversation_notif class
class :conversation-notif extends :x:element
{
    use XHPHelpers;
    use XHPReact;

    attribute
    :xhp:html-element,
    string notif @required,
    string src_username @required;

    protected function render(): XHPRoot
    {
        $this->constructReactInstance(
        'Conversation_notif',
            Map {
                'notif' => $this->:notif,
                'src_username' => $this->:src_username,
            }
        );
        return <div id={$this->getID()} />;
    }
}


// Get back all user dest who message user src
$sql_username =
"SELECT dst.username AS user, dst.prez
FROM messages
INNER JOIN users src ON src.id = messages.id_user_src
INNER JOIN users dst ON dst.id = messages.id_user_dst
WHERE messages.active = 'Y' AND src.username = '" . $_SESSION['auth']['username'] . "'
GROUP BY src.id, dst.id
UNION
SELECT src.username AS user, src.prez
FROM messages
INNER JOIN users src ON src.id = messages.id_user_src
INNER JOIN users dst ON dst.id = messages.id_user_dst
WHERE messages.active = 'Y' AND dst.username = '" . $_SESSION['auth']['username'] . "'
GROUP BY src.id, dst.id";

// Execute request
$sqlq_username = $PDO->query($sql_username);

// Put each messages on js-scope
$messages_list = <div />;

while (!empty($sqlr_user = $sqlq_username->fetch(PDO::FETCH_ASSOC)))
{
    $sqlr_username = $sqlr_user['user'];
    $profile_prez = $sqlr_user['prez'];
    $id_src = $PDO->query("SELECT id FROM users WHERE username = '" . $_SESSION['auth']['username'] . "'")->fetchColumn();
    $id_dst = $PDO->query("SELECT id FROM users WHERE username = '$sqlr_username'")->fetchColumn();
    if ($id_dst === false)
        $id_dst = null;
    else
        $id_dst = $id_dst;

    // Get back all message from and to current user
    $sql_messages_list =
    "SELECT messages.id, src.username AS src_username, dst.username AS dst_username,
    body, status, messages.active
    FROM messages
    INNER JOIN users src ON src.id = messages.id_user_src
    INNER JOIN users dst ON dst.id = messages.id_user_dst
    WHERE ((src.username = '" . $_SESSION['auth']['username'] . "'
        AND dst.username = '$sqlr_username')
    OR
        (dst.username = '" . $_SESSION['auth']['username'] . "'
        AND src.username = '$sqlr_username'))
    AND messages.active = 'Y'
    ORDER BY messages.creation_date";

    // Execute request
    $sqlq_messages_list = $PDO->query($sql_messages_list);


    $sqlr_messages_list = $sqlq_messages_list->fetchAll(PDO::FETCH_ASSOC);
    $messages_list->appendChild(<Conversation   messages_list={$sqlr_messages_list}
                                                dst_username={$sqlr_username}
                                                src_username={$_SESSION['auth']['username']}
                                                id_dst_username={$id_dst}
                                                id_src_username={$id_src}
                                                profile_prez={$profile_prez}
                                                />);
}

// Execute request
$matches_list = <div />;

// Get back all id user matches
if(!empty($sqlr_user_matches = $PDO->query("SELECT id_matchs FROM users WHERE username = '" . $_SESSION['auth']['username'] . "'")->fetchColumn()))
{
    $sqlr_user_list = $PDO->query("SELECT id, username, prez FROM users WHERE id IN ($sqlr_user_matches)");

    while ($sqlr_user = $sqlr_user_list->fetch(PDO::FETCH_ASSOC))
    {
        // var_dump($sqlr_user);
        $matched_user = $sqlr_user['username'];
        $profile_prez = $sqlr_user['prez'];
        $matched_id   = (int)$sqlr_user['id'];
        $matches_list->appendChild(<Matches profile_prez={$profile_prez}
                                            id_matched={$matched_id}
                                            user_matched={$matched_user}
                                            user_src={$_SESSION['auth']['username']} />);
    }
}

// Message page XML code
$message_page =
<body>
    <div class="match_list animated fadeIn">
        <x:js-scope>
            {$matches_list}
        </x:js-scope>
    </div>
    <div class="chat animated fadeIn" id="big">
        <x:js-scope>
            {$messages_list}
        </x:js-scope>
    </div>
    <x:js-scope>
        <conversation-notif notif={'1'}
                            src_username={$_SESSION['auth']['username']}
        />
    </x:js-scope>
</body>;

// Clear variable
// unset($sqlr_matches_list);

// Clear variable
unset($sqlr_messages_list);

// Display page
echo $message_page;
